import 'package:flutter/material.dart';
import 'package:ugalav/models/listing_category.dart';
import 'package:ugalav/models/locations.dart';
import 'dart:convert';
import 'dart:io';
import 'package:ugalav/utils/constants.dart' as constants;
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:ugalav/models/listings.dart';
import 'package:geolocator/geolocator.dart';

class AddListingPage extends StatefulWidget {
  @override
  _AddListingPageState createState() => _AddListingPageState();
}

class _AddListingPageState extends State<AddListingPage> {
  Listings _listing;

  final _formKey = GlobalKey <FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _isSubmitting = false;
  File _image;
  List<DropdownMenuItem<String>> _categories = [];
  List<DropdownMenuItem<String>> _locations = [];
  String _name, _contact, _category, _phone, _website, _email, _city, _street;
  String _photo, _brief, _location, _apt, _house, _zip, _state;
  String _lat = '';
  String _longt = '';
  bool _atLocation = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCategories();
    _getLocations();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Add Business"),
        ),
        body: Container(
            padding: EdgeInsets.all(5.0),
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: this._isSubmitting ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator()
                  ],
                ) : Form(
                    key: _formKey,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Business Name", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showNameInput(),
                          SizedBox(height: 5,),
                          Text("Contact Name", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showContactInput(),
                          SizedBox(height: 5,),
                          Text("Category", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showCategoryInput(),
                          SizedBox(height: 5,),
                          Text("Business Address", style: TextStyle(fontWeight: FontWeight.bold),),
                          _businessAddress(),
                          _locationBox(),
                          SizedBox(height: 5,),
                          Text("Business Phone", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showPhoneInput(),
                          SizedBox(height: 5,),
                          Text("Email", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showEmailInput(),
                          SizedBox(height: 5,),
                          Text("Website", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showWebsiteInput(),
                          SizedBox(height: 5,),
                          _showBriefInput(),
                          //_imagePicker(),
                          _showFormActions()
                        ])),
              ),
            ))
    );
  }


  Widget _showNameInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 5),
        child: TextFormField(
            onSaved: (val) => this._name = val,
            validator: (val) => val.length < 3 ? 'Invalid Business Name': null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: false,
              hintText: 'Enter a Business Name',
              //hintStyle: TextStyle(fontSize: 12)
            )));
  }

  Widget _showContactInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: TextFormField(
            onSaved: (val) => _contact = val,
            validator: (val) =>
            val.length < 3
                ? 'Invalid Name Enter Fullname'
                : null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Contact Person',
              hintText: 'Enter Contact Person Name',
            )));
  }

  Widget _showCategoryInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: DropdownButtonFormField <String>(
          value: _category,

          items: _categories
          ,
          //hint: Text('Category'),
          onChanged: (String newValue) {
            setState(() {
              _category = newValue;
            });
          },
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.all(2.0),
            hintText: 'Choose a category',
          ),

        ));
  }

  Widget _showLocationInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: DropdownButtonFormField <String>(
          value: _location,
          isExpanded: true,
          items: _locations,
          //hint: Text('Category'),
          onChanged: (String newValue) {
            setState(() {
              _location = newValue;
            });
          },
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.all(2.0),
            hintText: 'Choose a Location',
          ),

        ));
  }

  Widget _businessAddress(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        //_showLocationInput(),
        _showStateInput(),
        SizedBox(height: 5,),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(child: _showCityInput(), flex: 2,),
            Flexible(child: _showZipInput(), flex: 1,),
          ],
        ),
        SizedBox(height: 5,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Flexible(child: _showHouseInput(), flex: 1,),
            Flexible(child: _showStreetInput(), flex: 2,),
            Flexible(child: _showAptInput(),flex: 1,)
          ],
        ),


      ],
    );
  }

  Widget _showPhoneInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: TextFormField(
            onSaved: (val) => _phone = val,
            keyboardType: TextInputType.phone,
            validator: (val) =>
            val.length < 9
                ? 'Invalid Phone Number'
                : null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Mobile Number',
              hintText: 'Enter a valid Phone Number',
            )));
  }

  Widget _showWebsiteInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: TextFormField(
            onSaved: (val) => _website = val,
            validator: (val) => val.length < 9 ? 'Invalid URL' : null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Website',
              hintText: 'Enter Website address',
            )));
  }

  Widget _showEmailInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: TextFormField(
            onSaved: (val) => _email = val,
            validator: (val) =>
            val.length < 9
                ? 'Invalid Email Address'
                : null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Email Address',
              hintText: 'Enter a valid Email',
            )));
  }

  Widget _showStreetInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0),
        child: TextFormField(
            onSaved: (val) => _street = val,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(1),
              //labelText: 'Street',
              hintText: 'Street',

            )));
  }
  Widget _showAptInput() {
    return Padding(
        padding: EdgeInsets.only(left: 2.0),
        child: TextFormField(
            onSaved: (val) => _apt = val,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(1),
              //labelText: 'Street',
              hintText: 'Apt#',

            )));
  }
  Widget _showHouseInput() {
    return Padding(
        padding: EdgeInsets.only(right: 4),
        child: TextFormField(
            onSaved: (val) => _house = val,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(1),
              //labelText: 'Street',
              hintText: 'House',

            )));
  }

  Widget _showZipInput() {
    return Padding(
        padding: EdgeInsets.only(left: 2.0),
        child: TextFormField(
            onSaved: (val) => _zip = val,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(1),
              //labelText: 'Street',
              hintText: 'Zip#',

            )));
  }

  Widget _showBriefInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _brief = val,
            maxLines: 5,
            maxLength: 500,
            //validator: (val) => val.length < 3 ? 'Invalid Address' : null,
            decoration: InputDecoration(

              border: OutlineInputBorder(),
              hintText: 'Tell Something about your Business',

            )));
  }

  Widget _showStateInput() {
    return DropdownButtonFormField <String>(
      value: _state,
      isExpanded: true,
      items: constants.states.map<DropdownMenuItem<String>>(
              (String value) {
            return DropdownMenuItem<
                String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
      //hint: Text('City'),
      onChanged: (String newValue) {
        setState(() {
          _state = newValue;
        });
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        contentPadding: EdgeInsets.all(2.0),
        hintText: 'State',
      ),

    );
  }

  Widget _showCityInput() {
    return Padding(
        padding: EdgeInsets.only(right: 3.0),
        child: DropdownButtonFormField <String>(
          value: _city,
          isExpanded: true,
          items: constants.cities.map<DropdownMenuItem<String>>(
                  (String value) {
                return DropdownMenuItem<
                    String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
          // hint: Text('City'),
          onChanged: (String newValue) {
            setState(() {
              _city = newValue;
            });
          },
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.all(2.0),
            hintText: 'City',
          ),

        ));
  }

  Widget _locationBox(){
    return Row(
      children: [
        Text("I am Currently at the Location"),
        Checkbox(
            value: this._atLocation,
            onChanged: (bool value){
              setState(() {
                _atLocation = value;
              });
              if(value==true){
                _determinePosition();
              }else{
                setState(() {
                  _longt = '';
                  _lat = '';
                });
              }
            }
        )
      ],
    );
  }


  Widget _showFormActions() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _isSubmitting == true ? Container(
                width: 50,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Theme
                      .of(context)
                      .accentColor),

                ),
              ) : RaisedButton(
                  padding: EdgeInsets.all(15.0),
                  child: Text('Post Listing',
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .copyWith(color: Colors.white)),
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  color: Theme
                      .of(context)
                      .accentColor,
                  onPressed: _submit),

            ]));
  }

  void _submit() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {
        _isSubmitting = true;
      });
      uploadAndSave();
    }
  }

  _errorMessage(String msg){
    SweetAlert.show(context,
        title: "Ops",
        subtitle: msg,
        style: SweetAlertStyle.error,
        onPress: (bool choice){
          //Navigator.pop(context);
          return true;
        });
    setState(() {
      _isSubmitting = false;
    });
  }

  _popAndView(){
    //remove form stack
    Navigator.pop(context);
    SweetAlert.show(context,
        title: "Listing has been added",
        subtitle: "Please navigate to Listings page to see it",
        style: SweetAlertStyle.success,
        onPress: (bool choice){
          return true;
        });

    Navigator.pop(context);
    Navigator.of(context).pushReplacementNamed('/success-listing');
    /*Navigator.push(context, MaterialPageRoute(
        builder: (_) => ListingDetailPage(item: this._listing)
    ));*/

  }

  Future<int> uploadAndSave() async {
    var url = constants.BASE_URL + '/listings/';

    //var filename = _photo;
    final prefs = await SharedPreferences.getInstance();
    String storedUser = prefs.getString('jwt');
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + storedUser,
      'Content-type' : 'application/json',
    };
    print(headers);
    http.Response response = await http.post(Uri.parse(url), body: json.encode(
        {'title': this._name,
          'body': this._brief,
          'category': this._category,
          'contact': this._contact,
          'phone': this._phone,
          'website': this._website,
          'email': this._email,
          'city': this._city,
          'street': this._street,
          'state': this._state,
          'house': this._house,
          'apt': this._apt,
          'zipcode': this._zip,
          'location': this._location,
          'lat': this._lat,
          'longt': this._longt,
        }), headers: headers);
    final responseData = json.decode(response.body);

    if(response.statusCode == 201 || response.statusCode ==200){
      this._listing = Listings.fromJson(responseData);
        _popAndView();
    }else if(response.statusCode == 400){
      setState(() => _isSubmitting = false);
      _errorMessage(responseData['details']);
    }else{
      setState(() => _isSubmitting = false);
       _errorMessage(responseData['detail']);
    }
  }

  _getCategories() async {
    List<ListingCategory> categories = await fetchListingCategories();
    List<DropdownMenuItem<String>> catList = [];
    for(var i = 0; i < categories.length; i++){
      catList.add(
          DropdownMenuItem<
              String>(
            value: categories[i].name,
            child: Text(categories[i].name),
          )
      );
    }

    setState(() {
      _categories = catList;
    });
  }

  _getLocations() async {
    List<Location> locs = await fetchLocations();
    List<DropdownMenuItem<String>> catList = [];

    for(var i = 0; i < locs.length; i++){
      catList.add(
          DropdownMenuItem<
              String>(
            value: locs[i].name,
            child: Text(locs[i].name),
          )
      );
    }


    setState(() {
      _locations = catList;
    });
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }
    Position data = await Geolocator.getCurrentPosition();
    setState(() {
      _lat = data.latitude.toString();
      _longt = data.longitude.toString();
    });
    return data;
  }
}
