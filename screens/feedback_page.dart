import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ugalav/utils/constants.dart' as constants;

class FeedbackForm extends StatefulWidget {
  @override
  _FeedbackFormState createState() => _FeedbackFormState();
}

class _FeedbackFormState extends State<FeedbackForm> {

  final _formKey = GlobalKey <FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _isSubmitting = false;
  String _details, _title, _email;

  Widget _showFormActions() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _isSubmitting == true ? Container(
                width: 50,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Theme
                      .of(context)
                      .accentColor),

                ),
              ) : RaisedButton(
                  padding: EdgeInsets.all(15.0),
                  child: Text('Send Message',
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .copyWith(color: Colors.white)),
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  color: Theme
                      .of(context)
                      .accentColor,
                  onPressed: _submit),

            ]));
  }

  Widget _showBriefInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _details = val,
            maxLines: 5,
            maxLength: 1000,
            //validator: (val) => val.length < 3 ? 'Invalid Address' : null,
            decoration: InputDecoration(

              border: OutlineInputBorder(),
              hintText: 'Share some feed back with us',

            )));
  }

  Widget _showTitleInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _title = val,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Title',
              hintText: 'Enter Subject',
            )));
  }

  Widget _showEmailInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: TextFormField(
            onSaved: (val) => _email = val,
            validator: (val) =>
            val.length < 9
                ? 'Invalid Email Address'
                : null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Email Address',
              hintText: 'Enter a valid Email',
            )));
  }

  void _submit() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {
        _isSubmitting = true;
      });
      _sendMessage();
    }
  }

  Widget _FormFields(){
    return Form(
        key: _formKey,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Your Email", style: TextStyle(fontWeight: FontWeight.bold),),
              _showEmailInput(),
              SizedBox(height: 5,),
              Text("Subject", style: TextStyle(fontWeight: FontWeight.bold),),
              _showTitleInput(),
              SizedBox(height: 5,),
              Text("Comment", style: TextStyle(fontWeight: FontWeight.bold),),
              _showBriefInput(),
              _showFormActions()
            ]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Feedback Form"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: _isSubmitting ? Container(
            child: Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Theme
                    .of(context)
                    .accentColor),
              ),
            ),
          ) : _FormFields(),
        ),
      ),
    );
  }

  void _sendMessage() async {
    setState(() => _isSubmitting = true);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    http.Response response = await http.post(
        constants.BASE_URL + '/feedback/',
        body: {
          "email": _email,
          "comment": _details
        }
    );

    setState(() => _isSubmitting = false);
    if (response.statusCode == 200) {
      final snackbar = SnackBar(
          content: Text('Your Feedback has been Posted Successfully.', style: TextStyle(color: Colors.green))
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);

    }
  }
}
