
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ugalav/screens/add_classified_page.dart';
import 'package:ugalav/screens/add_listing_page.dart';
import 'package:ugalav/screens/feedback_page.dart';
import 'package:ugalav/screens/user_classifieds_page.dart';
import 'package:ugalav/screens/user_listings_page.dart';
import 'package:url_launcher/url_launcher.dart';

class PolicyPage extends StatefulWidget {
  @override
  _PolicyPageState createState() => _PolicyPageState();
}

class _PolicyPageState extends State<PolicyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Policy"),
      ),
      body: Container(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  SizedBox(height: 15.0,),

                  ListTile(
                    leading: Icon(Icons.policy_outlined),
                    title: Text('Privacy Policy'),
                    onTap: () => _launchURL('https://ugalav.com/privacy.html'),
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.wysiwyg_rounded),
                    title: Text('Disclaimer'),
                    onTap: () => _launchURL('https://ugalav.com/disclaimer.html')
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.article_outlined),
                    title: Text('FAQs'),
                    onTap: () => _launchURL('https://ugalav.com/faqs.html')
                  ),
                  Divider(),

                ],
              )
            ),

          ],
        ),
      ),
    );
  }
  void _launchURL(String url) async {

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
