
import 'package:flutter/material.dart';
import 'package:ugalav/models/listing_category.dart';
import 'dart:convert';
import 'dart:io';
import 'package:ugalav/utils/constants.dart' as constants;
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:ugalav/widgets/header_card.dart';
import 'package:ugalav/models/listings.dart';

class EditListingPage extends StatefulWidget {
  final Listings item;
  EditListingPage({Key key, this.item}): super(key: key);
  @override
  _EditListingPageState createState() => _EditListingPageState(item);
}

class _EditListingPageState extends State<EditListingPage> {
  final Listings _listing;

  _EditListingPageState(this._listing);

  final _formKey = GlobalKey <FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<DropdownMenuItem<String>> _categories = [];
  bool _isSubmitting = false;
  File _image;
  String _photo;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Business Form"),
        ),
        body: Container(
            padding: EdgeInsets.all(5.0),
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: this._isSubmitting ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator()
                  ],
                ) : Form(
                    key: _formKey,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Business Name", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showNameInput(),
                          SizedBox(height: 5,),
                          Text("Contact Name", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showContactInput(),
                          SizedBox(height: 5,),
                          Text("Category", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showCategoryInput(),
                          SizedBox(height: 5,),
                          Text("Business Address", style: TextStyle(fontWeight: FontWeight.bold),),
                          _businessAddress(),
                          SizedBox(height: 5,),
                          Text("Business Phone", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showPhoneInput(),
                          SizedBox(height: 5,),
                          Text("Email", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showEmailInput(),
                          SizedBox(height: 5,),
                          Text("Website", style: TextStyle(fontWeight: FontWeight.bold),),
                          _showWebsiteInput(),
                          _showBriefInput(),
                          _showFormActions()
                        ])),
              ),
            ))
    );
  }


  void _submit() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {
        _isSubmitting = true;
      });

      uploadAndSave().then((http_code) => {
            if(http_code == 200 || http_code ==201){
                _showList()
          }else{
            SweetAlert.show(context,
            title: "Ops!",
            subtitle: "Something went wrong Please try again later",
            style: SweetAlertStyle.error,
            onPress: (bool choice){
              setState(() {
                _isSubmitting = false;
              });
              return true;
            })
            }
      });


    }
  }

  _showList(){
    setState(() {
      _isSubmitting = false;
    });
    SweetAlert.show(context,
        title: "Success!",
        subtitle: "Your Business listing has been updated!",
        style: SweetAlertStyle.success,
        onPress: (bool choice){
          Navigator.pop(context);
          return true;
        });

  }

  Widget _showNameInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 5),
        child: TextFormField(
            onSaved: (val) => _listing.title = val,
            initialValue: _listing.title,
            validator: (val) => val.length < 3 ? 'Invalid Business Name': null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: false,
              hintText: 'Enter a Business Name',
              //hintStyle: TextStyle(fontSize: 12)
            )));
  }

  Widget _showContactInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: TextFormField(
            onSaved: (val) => _listing.contact = val,
            initialValue: _listing.contact,
            validator: (val) =>
            val.length < 3
                ? 'Invalid Name Enter Fullname'
                : null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Contact Person',
              hintText: 'Enter Contact Person Name',
            )));
  }

  Widget _showCategoryInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: DropdownButtonFormField <String>(
          value: _listing.category,
          items: _categories,
          //hint: Text('Category'),
          onChanged: (String newValue) {
            setState(() {
              _listing.category = newValue;
            });
          },
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.all(2.0),
            hintText: 'Choose a category',
          ),

        ));
  }

  Widget _businessAddress(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _showStateInput(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Flexible(child: _showHouseInput(), flex: 1,),
            Flexible(child: _showStreetInput(), flex: 2,),
            Flexible(child: _showAptInput(),flex: 1,)
          ],
        ),
        SizedBox(height: 5,),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(child: _showCityInput(), flex: 3,),
            Flexible(child: _showZipInput(), flex: 1,),
          ],
        )
      ],
    );
  }

  Widget _showPhoneInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: TextFormField(
            onSaved: (val) => _listing.phone = val,
            initialValue: _listing.phone,
            keyboardType: TextInputType.phone,
            validator: (val) =>
            val.length < 9
                ? 'Invalid Phone Number'
                : null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Mobile Number',
              hintText: 'Enter a valid Phone Number',
            )));
  }

  Widget _showWebsiteInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: TextFormField(
            onSaved: (val) => _listing.website = val,
            initialValue: _listing.website,
            validator: (val) => val.length < 9 ? 'Invalid URL' : null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Website',
              hintText: 'Enter Website address',
            )));
  }

  Widget _showEmailInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: TextFormField(
            onSaved: (val) => _listing.email = val,
            initialValue: _listing.email,
            validator: (val) =>
            val.length < 9
                ? 'Invalid Email Address'
                : null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Email Address',
              hintText: 'Enter a valid Email',
            )));
  }

  Widget _showStreetInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0),
        child: TextFormField(
            onSaved: (val) => _listing.street = val,
            initialValue: _listing.street,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(1),
              //labelText: 'Street',
              hintText: 'Street',

            )));
  }
  Widget _showAptInput() {
    return Padding(
        padding: EdgeInsets.only(left: 2.0),
        child: TextFormField(
            onSaved: (val) => _listing.apt = val,
            initialValue: _listing.apt,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(1),
              //labelText: 'Street',
              hintText: 'Apt#',

            )));
  }
  Widget _showHouseInput() {
    return Padding(
        padding: EdgeInsets.only(right: 4),
        child: TextFormField(
            onSaved: (val) => _listing.house = val,
            initialValue: _listing.house,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(1),
              //labelText: 'Street',
              hintText: 'House',

            )));
  }

  Widget _showZipInput() {
    return Padding(
        padding: EdgeInsets.only(left: 2.0),
        child: TextFormField(
            onSaved: (val) => _listing.zip = val,
            initialValue: _listing.zip,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(1),
              //labelText: 'Street',
              hintText: 'Zip#',

            )));
  }

  Widget _showBriefInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _listing.body = val,
            initialValue: _listing.body,
            maxLines: 5,
            //validator: (val) => val.length < 3 ? 'Invalid Address' : null,
            decoration: InputDecoration(

              border: OutlineInputBorder(),
              hintText: 'Tell Something about your Business',

            )));
  }

  Widget _showStateInput() {
    return DropdownButtonFormField <String>(
      value: _listing.state,
      isExpanded: true,
      items: constants.states.map<DropdownMenuItem<String>>(
              (String value) {
            return DropdownMenuItem<
                String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
      //hint: Text('City'),
      onChanged: (String newValue) {
        setState(() {
          _listing.state = newValue;
        });
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        contentPadding: EdgeInsets.all(2.0),
        hintText: 'State',
      ),

    );
  }

  Widget _showCityInput() {
    return Padding(
        padding: EdgeInsets.only(right: 3.0),
        child: DropdownButtonFormField <String>(
          value: _listing.city,
          isExpanded: true,
          items: constants.cities.map<DropdownMenuItem<String>>(
                  (String value) {
                return DropdownMenuItem<
                    String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
          // hint: Text('City'),
          onChanged: (String newValue) {
            setState(() {
              _listing.city = newValue;
            });
          },
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.all(2.0),
            hintText: 'City',
          ),

        ));
  }

  Widget _imagePicker(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          _image == null ? Text('No image selected.'): SizedBox(child:Image.file(_image) , width: 100, height: 80,),
          FlatButton(onPressed: () async {
            var file = await ImagePicker().getImage(source: ImageSource.gallery);

            setState(() {
              _image = File(file.path);
              _photo = file.path;
            });

            print(file.path);
            //var res = await uploadImage(file.path, widget.url);
          },
              child: Text("Select Photo")),
        ],
      ),
    );
  }

  Widget _showFormActions() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _isSubmitting == true ? Container(
                width: 50,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Theme
                      .of(context)
                      .accentColor),

                ),
              ) : RaisedButton(
                  padding: EdgeInsets.all(15.0),
                  child: Text('Save Listing',
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .copyWith(color: Colors.white)),
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  color: Theme
                      .of(context)
                      .accentColor,
                  onPressed: _submit),

            ]));
  }

  Future<int> uploadAndSave() async {
    var url = constants.BASE_URL + '/listings/' + this._listing.id.toString() +'/';
    print(url);
    final prefs = await SharedPreferences.getInstance();
    String storedUser = prefs.getString('jwt');
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + storedUser
    };

    var request = http.MultipartRequest('PUT', Uri.parse(url));
    request.headers.addAll(headers);
    request.fields['title'] = this._listing.title;
    request.fields['body'] = this._listing.body;
    request.fields['category'] = this._listing.category;
    request.fields['contact'] = this._listing.contact;
    request.fields['phone'] = this._listing.phone;
    request.fields['website'] = this._listing.website;
    request.fields['email'] = this._listing.email;
    request.fields['city'] = this._listing.city;
    request.fields['street'] = this._listing.street;
    request.fields['state'] = this._listing.state;
    request.fields['house'] = this._listing.house;
    request.fields['apt'] = this._listing.apt;
    request.fields['zipcode'] = this._listing.zip;
    request.fields['lat'] = '78.1234';
    request.fields['longt'] = '78.1234';
    var res = await request.send();
    print(res.statusCode);
    return res.statusCode;
  }

  _getCategories() async {
    List<ListingCategory> categories = await fetchListingCategories();
    List<DropdownMenuItem<String>> catList = [];
    for(var i = 0; i < categories.length; i++){
      catList.add(
          DropdownMenuItem<
              String>(
            value: categories[i].name,
            child: Text(categories[i].name),
          )
      );
    }

    setState(() {
      _categories = catList;
    });
  }
}
