
import 'package:flutter/material.dart';
import 'package:ugalav/models/locations.dart';
import 'package:ugalav/models/listings.dart';
import 'package:ugalav/screens/listing_detail_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:ugalav/utils/constants.dart' as constants;
import 'package:ugalav/widgets/FeaturedWidget.dart';

class ListingScreen extends StatefulWidget {
  @override
  _ListingScreenState createState() => _ListingScreenState();
}

class _ListingScreenState extends State<ListingScreen> with AutomaticKeepAliveClientMixin<ListingScreen> {

  @override
  bool get wantKeepAlive => true;

  List<Listings> _listings = [];
  List<DropdownMenuItem<String>> _locations = [];
  String _category;
  String _location;
  String q;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getListings();
  }

  Widget _listingsWidget(List<Listings> listings){
    return Expanded(
      child: ListView.separated(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          separatorBuilder: (context, index) => Divider(
            color: Colors.grey,
            height: 1.0,
          ),
          padding: EdgeInsets.all(0.0),
          itemCount: listings.length,
          itemBuilder:(_, index){
            return ListTile(
              isThreeLine: true,
              dense: true,
              leading: Icon(Icons.business_center, size: 45,),
              title: Hero(
                tag: 'listing' + listings[index].id.toString(),
                child: Text(listings[index].title,
                    style:
                    Theme.of(context).textTheme.subtitle2.copyWith(
                        fontWeight: FontWeight.bold,
                        fontSize: 15
                    ),
                    overflow: TextOverflow.ellipsis),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(

                    children: <Widget>[
                      Icon(Icons.location_on, color: Colors.green, size: 17.0,),
                      Text(' ' +
                          substring_text(listings[index].street + ' ' + listings[index].city, 26),
                        style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.grey),
                          overflow: TextOverflow.ellipsis,
                          softWrap: true,),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.phone, size: 17.0, color: Colors.grey),
                      Text(' ' +
                        listings[index].phone,
                        style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.red)),
                    ],
                  ),


                ],
              ),
                trailing: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.arrow_forward_ios),
                  ],
                ),

              onTap: () => Navigator.push(context, MaterialPageRoute(
                   builder: (_) => ListingDetailPage(item: listings[index],),
                 ))
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
           _searchListingWidget(context),
           _listWidget(context),
          FeaturedWidget(),
        ],
      ),
    );
  }

  Widget _listWidget(BuildContext context){
    if(_listings == null) {
      return Container(
        padding: EdgeInsets.all(30.0),
        child: Center(child: CircularProgressIndicator()),
      );
    }
    if(_listings != null && this._listings.length ==0){
        return Container(
          padding: EdgeInsets.all(15.0),
          child: Center(
            child: Text("No Businesses Found"),
          ),
        );
    }else{
      return _listingsWidget(_listings);
    }
  }

   Widget _searchListingWidget(BuildContext context){
        return Container(
          color: Theme.of(context).primaryColor,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  flex: 2,
                  child: Container(
                    width: 250,
                      height: 45,
                      child: _showLocationInput()
                  ),
                ),
                Flexible(
                  flex: 3,
                  child: Container(
                    height: 36,
                    width: 200,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      
                      borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    child: TextField(
                      
                      decoration: InputDecoration(
                          suffixIcon: Icon(Icons.search, color: Colors.green,),
                        border: OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                               const Radius.circular(10.0),
                            ),
                        ),
                        hintText: 'Type to search',
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        fillColor: Colors.white,
                        filled: true,
                        contentPadding:
                        EdgeInsets.only(left: 15, bottom: 1, top: 1, right: 15),
                      ),
                      onChanged: (value) => q = value,
                      onSubmitted: (value) => {
                        _searchListings()
                      },
                    ),
                  ),
                ),
               
              ],
            ),
          ),
        );
   }

  void _getListings() async{
    final List<Listings> response = await fetchListing(location: null, q: null);
    if(this.mounted) {
      setState(() {
        _listings = response;
      });
    }
  }
  void _searchListings() async{
    setState(() {
      _listings = null;
    });
    var location = _location !=null ? _location : '';
    var query = q !=null ? q : '';
    print("searching for " + query + ' and ' + location );

    final List<Listings> response = await fetchListing(location:location, q: query);
    setState(() {
      _listings = response;
    });
  }


  Widget _showLocationInput() {
    return Padding(
        padding: EdgeInsets.only(top: 2.0, bottom: 4),
        child: DropdownButtonFormField <String>(
          value: _location,
          isExpanded: true,
          items: constants.states.map<DropdownMenuItem<String>>(
                  (String value) {
                return DropdownMenuItem<
                    String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
          onChanged: (String newValue) {
            setState(() {
              _location = newValue;
            });
            print("changed State");
            _searchListings();
          },

          decoration: InputDecoration(
            fillColor: Colors.white,
            hintStyle: TextStyle(color: Colors.white),
            border: null,
            contentPadding: EdgeInsets.all(2.0),
            hintText: 'Location',
          ),

        ));
  }
}
