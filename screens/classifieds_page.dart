import 'package:flutter/material.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:ugalav/widgets/FeaturedWidget.dart';
import 'package:ugalav/widgets/classified_card.dart';
import 'package:ugalav/widgets/header_card.dart';

class ClassifiedsPage extends StatefulWidget {
  @override
  _ClassifiedsPageState createState() => _ClassifiedsPageState();
}

class _ClassifiedsPageState extends State<ClassifiedsPage>  with AutomaticKeepAliveClientMixin<ClassifiedsPage>{

  @override
  bool get wantKeepAlive => true;
  List<Classified> _classifieds;
  ScrollController _controller = new ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getClassifieds();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return RefreshIndicator(
      onRefresh: () => _refreshProducts(context),
      child: Container(
        color: Color.fromARGB(204, 204, 204, 204),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            HeaderCard('Community Forum'),
            _classfiedsList(_classifieds),
            FeaturedWidget()

          ],
        )),
    );
  }

  Widget _classfiedsList(List<Classified> classifieds){
    if(classifieds == null){
       return _loader();
    }
    if(classifieds.length == 0){
      return Center(child: Text("No Classifieds Found"),);
    }
    return Expanded(
      child: ListView.builder(
          physics: const AlwaysScrollableScrollPhysics(), // new
          controller: _controller,
          itemCount: classifieds.length,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.only(left:4.0, right: 4.0),
              child: ClassifiedCard(classifieds[index]),
            );
          }
      ),
    );
  }


  Widget _loader(){
    return Container(
      padding: EdgeInsets.all(30.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }

  Future<void> _refreshProducts(BuildContext context) async {
    return await _getClassifieds();
  }

  void _getClassifieds() async{
    final List<Classified> response = await fetchClassifieds();

    setState(() {
      _classifieds = response;
    });
  }
}
