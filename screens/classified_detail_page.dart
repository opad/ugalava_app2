
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/screens/classified_checkout_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:url_launcher/url_launcher.dart';

class ClassifiedDetailPage extends StatefulWidget {
  final Classified item;
  ClassifiedDetailPage({Key key, @required this.item}):super(key: key);

  @override
  _ClassifiedDetailPageState createState() => _ClassifiedDetailPageState(item);
}

class _ClassifiedDetailPageState extends State<ClassifiedDetailPage> {
  final Classified classified;

  _ClassifiedDetailPageState(this.classified);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Classified Details"),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(classified.title, style: Theme.of(context).textTheme.title.copyWith(fontWeight: FontWeight.bold)),
                Hero(
                  tag: 'photo' + classified.id.toString(),
                    child: Image(image: _photo(),)
                ),
                _card_text(context),
                SizedBox(height: 20,),
                Text(classified.detail),
                SizedBox(height: 20,),
                _shareBox(),

              ],
            ),
          ),
        ),
    );
  }


  Widget _card_text(BuildContext context){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Icon(Icons.access_time, size: 12, color: Colors.grey,),
              ),
              Text(formatDate(this.classified.created), style: Theme.of(context).textTheme.caption),
            ],
          ),
          SizedBox(height: 10,),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Icon(Icons.phone, size: 15, color: Colors.grey,),
              ),
              InkWell(
                onTap: () => _launchURL('tel:' + this.classified.phone),
                child: Text(this.classified.phone ?? 'No Contact', style: Theme.of(context).
                textTheme.subtitle2.copyWith(color: Colors.red, fontWeight: FontWeight.bold)),
              ),

            ],
          ),
          SizedBox(height: 10,),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Icon(Icons.add_shopping_cart_sharp, size: 15, color: Colors.grey,),
              ),
              Text('\$' + this.classified.price, style: Theme.of(context).textTheme.subtitle2),
            ],
          ),
        ],
      ),
    );
  }

  Widget _shareBox(){
    var phone = classified.phone ?? 'No Number';
    String body = 'Ugalav classified: ' + classified.title + '\n' + phone;
    return RaisedButton(
        child: Text("Share"),
        onPressed: () => {
          Share.share(body, subject:'Ugalav Shared a classified')
        });
  }

  void _launchURL(String url) async {

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print('Could not launch $url');
    }
  }

  ImageProvider _photo(){
    return classified.photo == null ? AssetImage('assets/images/no-image-icon.jpg') : NetworkImage(classified.photo);
  }

}
