import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ugalav/utils/constants.dart' as constants;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ugalav/utils/utility_funcs.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  final _formKey = GlobalKey <FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _isSubmitting = false;
  bool _obscureText = true;
  String _username, _password, _fullname, _phone, _email;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: [
                _loginForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _loginForm(){
    return Form(
        key: _formKey,
        child: Column(children: [
          _showTitle(),
          _showFullnameInput(),
          _showEmailInput(),
          _showUsernameInput(),
          _showPasswordInput(),
          _showFormActions()
        ]));
  }

  Widget _showTitle() {
    return Column(
      children: <Widget>[
        Image(image: AssetImage('assets/images/logo_small.png'),),
        SizedBox(height: 14.0,),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('Register', style: Theme.of(context).textTheme.title.copyWith(color: Colors.white)),
        ),
      ],
    );
  }


  Widget _showFullnameInput() {
    return Padding(
        padding: EdgeInsets.only(top: 5.0),
        child: TextFormField(
            onSaved: (val) => _fullname = val,
            //keyboardType: TextInputType.phone,
            validator: (val) => val.split(" ").length >=2 ? null: 'Enter both names separated by space',
            cursorColor: Colors.white,
            decoration: InputDecoration(
              //contentPadding: EdgeInsets.all(4.0),
              filled: true,
              border: OutlineInputBorder(),
              fillColor: Colors.white,
              labelText: 'Name',
              hintText: 'Enter Your Fullname',
            )));
  }

  Widget _showPhoneInput() {
    return Padding(
        padding: EdgeInsets.only(top: 5.0),
        child: TextFormField(
            onSaved: (val) => _phone = val,
            keyboardType: TextInputType.phone,
            validator: (val) => val.length < 12 ? 'Invalid Phone': null,
            cursorColor: Colors.white,
            decoration: InputDecoration(
              //contentPadding: EdgeInsets.all(4.0),
              filled: true,
              border: OutlineInputBorder(),
              fillColor: Colors.white,
              labelText: 'Phone',
              hintText: 'Enter Your Phone',
            )));
  }

  Widget _showEmailInput() {
    return Padding(
        padding: EdgeInsets.only(top: 5.0),
        child: TextFormField(
            onSaved: (val) => _email = val,
            keyboardType: TextInputType.emailAddress,
            validator: (val) =>  validateEmail(val),
            cursorColor: Colors.white,
            decoration: InputDecoration(
              //contentPadding: EdgeInsets.all(4.0),
              filled: true,
              border: OutlineInputBorder(),
              fillColor: Colors.white,
              labelText: 'Email Address',
              hintText: 'Enter Email Address',
            )));
  }

  Widget _showUsernameInput() {
    return Padding(
        padding: EdgeInsets.only(top: 5.0),
        child: TextFormField(
            onSaved: (val) => _username = val,
            //keyboardType: TextInputType.phone,
            validator: (val) => val.length < 3 ? 'Invalid Username': null,
            cursorColor: Colors.white,
            decoration: InputDecoration(
              //contentPadding: EdgeInsets.all(4.0),
              filled: true,
              border: OutlineInputBorder(),
              fillColor: Colors.white,
              labelText: 'Username',
              hintText: 'Enter Your Username',
            )));
  }

  Widget _showPasswordInput() {
    return Padding(
        padding: EdgeInsets.only(top: 5.0),
        child: TextFormField(
            onSaved: (val) => _password = val,
            validator: (val) => val.length < 8 ? 'Password too short': null,
            obscureText: _obscureText,
            decoration: InputDecoration(
              suffixIcon: GestureDetector(
                onTap: (){
                  setState(() => _obscureText = !_obscureText);
                },
                child: Icon(_obscureText ? Icons.visibility: Icons.visibility_off),
              ),
              border: OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
              labelText: 'Password',
              hintText: 'Enter password, min length 8',
            )));
  }

  Widget _showFormActions() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _isSubmitting == true ? Container(
                width: 50,
                child: LinearProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Theme.of(context).accentColor),
                ) ,
              ): RaisedButton(
                  padding: EdgeInsets.all(15.0),
                  child: Text('Register',
                      style: Theme.of(context)
                          .textTheme
                          .body1
                          .copyWith(color: Colors.white)),
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  color: Colors.green,
                  onPressed: _submit),

               FlatButton(
                  padding: EdgeInsets.all(20),
                  child: Text('I am already Registered', style: TextStyle(decoration: TextDecoration.underline,),),
                  onPressed: () => Navigator.pushReplacementNamed(context, '/login'))
            ]));
  }

  void _submit(){
    final form = _formKey.currentState;
    if(form.validate()){
      form.save();
      _RegisterUser();
    }
  }

  void _RegisterUser() async {
    setState(() => _isSubmitting = true);
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Accept': 'application/json',
    };
    var names = _fullname.split(" ");
    http.Response response = await http.post(constants.BASE_URL + '/users/',
        body: json.encode({
          "first_name": names[0],
          "last_name": names[1],
          "email": _email,
          "username": _username,
          "password": _password
        }), headers: headers
    );
    final responseData = json.decode(response.body);

    print(responseData);
    print(response.statusCode);
    if(response.statusCode == 200 || response.statusCode == 201){
      _loginUser();

    }else if(response.statusCode == 400 ){
       setState(() => _isSubmitting = false);
       if(responseData.containsKey('username')){
         _showErrorSnack(responseData['username'][0]);
       }else if(responseData.containsKey('email')){
         _showErrorSnack(responseData['email'][0]);
       }
    }else{
      setState(() => _isSubmitting = false);
      String errorMsg = responseData['detail'];
      _showErrorSnack(errorMsg);
    }


  }

  void _loginUser() async {
    setState(() => _isSubmitting = true);
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Accept': 'application/json',
    };
    http.Response response = await http.post(constants.BASE_URL + '/token/',
        body: json.encode({
          "username": _username,
          "password": _password
        }), headers: headers
    );
    final responseData = json.decode(response.body);
    if(response.statusCode == 200){
      setState(() => _isSubmitting = false);
      print(responseData);
      _storeUserData(responseData);
      _showSuccessSnack();
      _redirectUser();

    }else{
      setState(() => _isSubmitting = false);
      String errorMsg = responseData['message'];
      _showErrorSnack(errorMsg);
    }


  }

  void _storeUserData(responseData) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('jwt',responseData['access']);
    print(responseData['access']);
  }

  void _showSuccessSnack(){
    final snackbar = SnackBar(
        content: Text('User successfully Logged In', style: TextStyle(color: Colors.green))
    );

    _scaffoldKey.currentState.showSnackBar(snackbar);
    _formKey.currentState.reset();
  }

  void _showErrorSnack(String errorMsg){
    final snackbar = SnackBar(
        content: Text(errorMsg, style: TextStyle(color: Colors.red))
    );

    _scaffoldKey.currentState.showSnackBar(snackbar);
    throw Exception('Error Loggin: $errorMsg');
  }

  void _redirectUser(){
    Future.delayed(Duration(seconds: 2), () {
      Navigator.pushReplacementNamed(context, '/home');

    });
  }
}
