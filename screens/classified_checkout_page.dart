
import 'package:flutter/material.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/screens/stripe_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:ugalav/widgets/classified_card.dart';

class ClassifiedCheckout extends StatefulWidget {
  final Classified item;
  ClassifiedCheckout({Key key, @required this.item}):super(key: key);
  @override
  _ClassifiedCheckoutState createState() => _ClassifiedCheckoutState(item);
}

class _ClassifiedCheckoutState extends State<ClassifiedCheckout> {
  final Classified _classified;
  _ClassifiedCheckoutState(this._classified);

  int selectedRadioTile;
  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text("Checkout"),
       ),
      body: Container(
        padding: EdgeInsets.only(top:50.0, right: 10, left: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text("You are posting to the Forum. Please complete posting by making a payment"),
            _productSummary(),
            _chooseProduct(),
            _checkout(),
        SizedBox(height: 20,),
        RaisedButton(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text("Cancel and Go Home", style: TextStyle(color: Colors.white),),
            ),
            color: Colors.red,
            onPressed: () => Navigator.pushNamed(context, '/home')

            ),
          ],
        )
      ),
    );
  }

  Widget _productSummary(){

    return ClassifiedCard(_classified);

  }

  Widget _chooseProduct(){
     return Container(
       padding: EdgeInsets.all(10),
       child: Column(
         children: [
           RadioListTile(
               value: 1,
               groupValue: selectedRadioTile,
               title: Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                   Text("FREE POSTING"),
                   Text("Trial Period only", style: Theme.of(context).textTheme.caption,)
                 ],
               ),
               onChanged: (val) => setSelectedRadioTile(val),
             activeColor: Colors.green,
           ),
          /* RadioListTile(
               value: 2,
               groupValue: selectedRadioTile,
               title: Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                   Text("ADVERTISEMENT \$5 monthly"),
                   Text("Trial period - ad will also appear at ugalav.com", style: Theme.of(context).textTheme.caption,)
                 ],
               ),
               onChanged: (val) => setSelectedRadioTile(val),
             activeColor: Colors.blueAccent,
           ),*/
           SizedBox(
             height: 30,
           )
         ],
       ),
     );
  }

  Widget _checkout(){
    return RaisedButton(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Text("Continue", style: TextStyle(color: Colors.white, fontSize: 16.0),),
      ),
        color: Colors.blue,
        onPressed: () async {
          if(selectedRadioTile == 1){
              Navigator.of(context).pushReplacementNamed('/success');
          }
          final sessionId = await fetchSessionId(this._classified.id.toString(), selectedRadioTile);
          Navigator.push(context, MaterialPageRoute(
            builder: (_) => CheckoutPage(sessionId: sessionId,),
          ));
        });
  }

}
