
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:ugalav/models/categories.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/screens/classified_checkout_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:ugalav/widgets/header.dart';
import 'package:ugalav/utils/constants.dart' as constants;
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddClassified extends StatefulWidget {
  final String category;
  AddClassified({Key key, this.category}):super(key: key);
  @override
  _AddClassifiedState createState() => _AddClassifiedState(category);
}

class _AddClassifiedState extends State<AddClassified> {
  String _cat;
  _AddClassifiedState(this._cat);

  final _formKey = GlobalKey <FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _isSubmitting = false;
  Classified _classified;
  File _image;
  List<DropdownMenuItem<String>> _categories = [];
  //String _cat;
  String _title, _details, _phone, _price, _negotiable, _period, _photo;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCategories();
  }

  _getCategories() async {
     List<Category> categories = await fetchCategories();
     List<DropdownMenuItem<String>> catList = [];
     for(var i = 0; i < categories.length; i++){
       catList.add(
           DropdownMenuItem<
               String>(
             value: categories[i].id.toString(),
             child: Text(categories[i].name),
           )
       );
     }

     setState(() {
       _categories = catList;
     });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
       appBar: AppBar(
         title: _cat == '3' ? Text("Post A Job") : Text("Post to Forum"),
       ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              this._isSubmitting ? _loader() : _FormFields()
            ],
          ),
        ),
      ),
    );
  }

  Widget _loader(){
    return Container(
      padding: EdgeInsets.all(30.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }

  Widget _FormFields(){
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
            children: [
          Text("Title", style: TextStyle(fontWeight: FontWeight.bold),),
          _showTitleInput(),
          SizedBox(height: 5,),
          Text("Contact Phone", style: TextStyle(fontWeight: FontWeight.bold),),
          _showPhoneInput(),
              SizedBox(height: 5,),
              Text("Category", style: TextStyle(fontWeight: FontWeight.bold),),
              _showCategoryInput(),
          SizedBox(height: 5,),
          Text("Price", style: TextStyle(fontWeight: FontWeight.bold),),
          _showPriceInput(),
              SizedBox(height: 5,),

              _cat == '3' ?  Text("Billing Period",
                  style: TextStyle(fontWeight: FontWeight.bold),): SizedBox(height: 1,),
              _cat == '3' ? _showPeriodInput(): SizedBox(height: 1,),

          SizedBox(height: 5,),
          _imagePicker(),
          _showBriefInput(),

          _showFormActions()
        ]));
  }

  Widget _showTitleInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _title = val,
            //validator: (val) => val.length < 9 ? 'Invalid Phone Number': null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Title',
              hintText: 'Enter Classified Title',
            )));
  }

  Widget _showPriceInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _price = val,
            //validator: (val) => val.length < 9 ? 'Invalid Phone Number': null,
            decoration: InputDecoration(
              prefixText: ' \$ ',
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Price',
              hintText: 'Enter item Price',
            )));
  }

  Widget _showPhoneInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _phone = val,
            keyboardType: TextInputType.phone,
            validator: (val) => val.length != 10 ? 'Invalid Phone Number': null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Phone',
              hintText: 'Enter a Contact Phone Number',
            )));
  }

  Widget _showNegotiableInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _negotiable = val,
            //validator: (val) => val.length < 9 ? 'Invalid Phone Number': null,
            decoration: InputDecoration(
              //contentPadding: EdgeInsets.all(4.0),
              border: OutlineInputBorder(),
              //labelText: 'Is Price fixed',
              hintText: 'Enter Price if it an item for sale',
            )));
  }

  Widget _showPeriodInput() {
    return Padding(
        padding: EdgeInsets.only(right: 3.0),
        child: DropdownButtonFormField <String>(
          value: _period,
          isExpanded: true,
          items: constants.billPeriods.map<DropdownMenuItem<String>>(
                  (String value) {
                return DropdownMenuItem<
                    String>(
                  value: value,
                  child: Text('Per ' + value),
                );
              }).toList(),
          // hint: Text('City'),
          onChanged: (String newValue) {
            setState(() {
              _period = newValue;
            });
          },
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.all(2.0),
            hintText: 'Billing Period',
          ),

        ));
  }


  Widget _showBriefInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _details = val,
            maxLines: 5,
            maxLength: 500,
            //validator: (val) => val.length < 3 ? 'Invalid Address' : null,
            decoration: InputDecoration(

              border: OutlineInputBorder(),
              hintText: 'Tell Something about your Business',

            )));
  }

  Widget _showCategoryInput() {
    return DropdownButtonFormField <String>(
      value: _cat,
      isExpanded: true,
      items: _categories,
      //hint: Text('City'),
      onChanged: (String newValue) {
        setState(() {
          _cat = newValue;
        });
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        contentPadding: EdgeInsets.all(2.0),
        hintText: 'Category',
      ),

    );
  }

  Widget _showFormActions() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _isSubmitting == true ? Container(
                width: 50,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Theme
                      .of(context)
                      .accentColor),

                ),
              ) : RaisedButton(
                  padding: EdgeInsets.all(15.0),
                  child: Text('Continue',
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .copyWith(color: Colors.white)),
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  color: Theme
                      .of(context)
                      .accentColor,
                  onPressed: _submit),

            ]));
  }

  Widget _imagePicker(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          _image == null ? Text('No image selected.'): SizedBox(child:Image.file(_image) , width: 100, height: 80,),
          FlatButton(onPressed: () => _showPicker(context),
              child: Text("Select Photo")),
        ],
      ),
    );
  }

  void _submit() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {
        _isSubmitting = true;
      });

       saveAndUpload().then((httpCode) =>
       {
         if(httpCode == 200 || httpCode == 201){
           if(this._classified !=null){
                _popAndCheckout()

           }
         }else if(httpCode == 401){
           _errorMessage("Your Session has Expired! \n Please login again")
          }else{
            _errorMessage("An Error has Occurred \nplease try again later.")
         }
       });
    }
  }

  _errorMessage(String msg){
    SweetAlert.show(context,
        title: "Ops",
        subtitle: msg,
        style: SweetAlertStyle.error,
        onPress: (bool choice){
          Navigator.pop(context);
          return true;
        });
      setState(() {
        _isSubmitting = false;
      });
  }

  _popAndCheckout(){
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(
        builder: (_) => ClassifiedCheckout(item: this._classified)
    ));
  }

  Future<int> saveAndUpload() async {

    var url = constants.BASE_URL + '/classifieds/';
    var filename = _photo;
    final prefs = await SharedPreferences.getInstance();
    String storedUser = prefs.getString('jwt');
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + storedUser
    };
    var request = http.MultipartRequest('POST', Uri.parse(url),);
    request.headers.addAll(headers);
    if(filename !=null){
      request.files.add(await http.MultipartFile.fromPath('photo', filename));
    }
    request.fields['title'] = this._title;
    request.fields['phone'] = this._phone;
    request.fields['expected_price'] = this._price;
    request.fields['detail'] = this._details;
    request.fields['is_price_negotiable'] = 'Y';
    request.fields['is_seller'] = 'Y';
    request.fields['is_individual'] = 'Y';
    request.fields['is_active'] = 'Y';
    request.fields['category'] = this._cat;
    request.fields['rate_period'] = this._period;
    var res = await request.send();
    var response = await http.Response.fromStream(res);
    if(response.statusCode ==201){
      var jsonData = jsonDecode(response.body);
       this._classified = Classified.fromJson(jsonData);
    }
    print(response.body);
    print(res.statusCode);
    return res.statusCode;
  }

  void _getImage(ImageSource imageSource) async {
    var file = await ImagePicker().getImage(source: imageSource, maxWidth: 800);
    if(file !=null) {
      setState(() {
        _image = File(file.path);
        _photo = file.path;
      });
    }
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _getImage(ImageSource.gallery);
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _getImage(ImageSource.camera);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
}
