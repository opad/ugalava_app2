import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ugalav/firebase_notification_handler.dart';

class Loader extends StatefulWidget {
  @override
  _LoaderState createState() => _LoaderState();
}

class _LoaderState extends State<Loader> with SingleTickerProviderStateMixin {
 bool _isLoading = true;
 Animation<double> animation;
 AnimationController controller;
  @override
  void initState() {
    super.initState();
    new FirebaseNotifications().setUpFirebase();
    checkIsLogin();
    controller = AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = Tween<double>(begin: 0, end: 250).animate(controller)
      ..addListener(() {
        setState(() {

        });
    });
    controller.forward();
  }
  Future<Null> checkIsLogin() async {
    String _token = "";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = prefs.getString('jwt');
   if (_token != "" && _token != null) {
     Future.delayed(Duration(seconds: 3), () {
       Navigator.pushReplacementNamed(context, '/home');

     });
    }else{
     Future.delayed(Duration(seconds: 3), () {
       Navigator.pushReplacementNamed(context, '/login');

     });
    }
    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: Center(
        child: Container(
          height: animation.value,
          width: animation.value,
          child: Image.asset('assets/images/logo_large.png'),
        ),
      ),
    );
  }

 @override
  void dispose() {
    // TODO: implement dispose
   controller.dispose();
    super.dispose();
  }
}
