
import 'package:flutter/material.dart';

class AdvertSuccessPage extends StatefulWidget {
  @override
  _AdvertSuccessPageState createState() => _AdvertSuccessPageState();
}

class _AdvertSuccessPageState extends State<AdvertSuccessPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Image.asset('assets/images/undraw_Post_online_re_1b82.png', height: 400,),
              Text("Your Advert has been posted Successfully",
                style: Theme.of(context).textTheme.subtitle2, textAlign: TextAlign.center,),
             SizedBox(height: 50,),
             RaisedButton(
               color: Colors.green,
                 onPressed: () => Navigator.of(context).pushReplacementNamed('/home'),
                child: Text("Go To Home Page", style: TextStyle(color: Colors.white),),
             )
            ],
          ),
        ),
      ),
    );
  }
}
