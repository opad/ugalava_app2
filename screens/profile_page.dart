
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ugalav/screens/add_advert_page.dart';
import 'package:ugalav/screens/add_classified_page.dart';
import 'package:ugalav/screens/add_listing_page.dart';
import 'package:ugalav/screens/feedback_page.dart';
import 'package:ugalav/screens/policy_page.dart';
import 'package:ugalav/screens/user_classifieds_page.dart';
import 'package:ugalav/screens/user_listings_page.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                    padding: EdgeInsets.all(18.0),
                    child: Text('Settings', style:TextStyle(fontSize: 24.0),)
                ),
                SizedBox(height: 15.0,),

                ListTile(
                  leading: Icon(Icons.business),
                  title: Text('My Business'),
                  subtitle: Text('Business posted by me'),
                  trailing: Icon(Icons.arrow_forward_ios),
                  onTap: () => Navigator.push(context, MaterialPageRoute(
                    builder: (_) => UserListings(),
                  )),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.chat_outlined),
                  title: Text('My Forum Posts'),
                  subtitle: Text('Forum items Posted by me'),
                  trailing: Icon(Icons.arrow_forward_ios),
                  onTap: () => Navigator.push(context, MaterialPageRoute(
                    builder: (_) => UserForum(),
                  )),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.ad_units_rounded),
                  title: Text('Advertise'),
                  subtitle: Text('Post an Advert'),
                  trailing: Icon(Icons.arrow_forward_ios),
                  onTap: () => Navigator.push(context, MaterialPageRoute(
                    builder: (_) => AddAdvert(),
                  )),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.headset_mic),
                  title: Text('UGALAV user Support'),
                  subtitle: Text('Contact us 24/7'),
                  trailing: Icon(Icons.arrow_forward_ios),
                  onTap: () => Navigator.push(context, MaterialPageRoute(
                    builder: (_) => FeedbackForm(),
                  )),
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.headset_mic),
                  title: Text('Privacy'),
                  subtitle: Text('Privacy Policy, Disclaimer'),
                  trailing: Icon(Icons.arrow_forward_ios),
                  onTap: () => Navigator.push(context, MaterialPageRoute(
                    builder: (_) => PolicyPage(),
                  )),
                ),
                Divider(),
                ListTile(
                  leading:Icon(Icons.exit_to_app) ,
                  title: Text('Log Out'),
                  onTap: () =>{
                      _logout(context)
                  },
                ),
                Center(
                  child: Column(
                    children: [
                      Text("From"),
                      Image.asset('assets/images/logo.png', width: 30,),
                    ],
                  ),
                )
              ],
            )
          ),

        ],
      ),
    ));
  }

  void _logout(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('jwt');
    Navigator.pushNamed(context, '/login');
  }
}
