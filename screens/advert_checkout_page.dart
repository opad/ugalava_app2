
import 'package:flutter/material.dart';
import 'package:ugalav/models/advert.dart';
import 'package:ugalav/screens/Advert_stripe_page.dart';
import 'package:ugalav/screens/stripe_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:ugalav/widgets/classified_card.dart';

class AdvertCheckout extends StatefulWidget {
  final Advert item;
  AdvertCheckout({Key key, @required this.item}):super(key: key);
  @override
  _AdvertCheckoutState createState() => _AdvertCheckoutState(item);
}

class _AdvertCheckoutState extends State<AdvertCheckout> {
  final Advert _advert;
  _AdvertCheckoutState(this._advert);

  bool _isSubmiting= false;

  int selectedRadioTile;
  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text("Checkout"),
       ),
      body: Container(
        padding: EdgeInsets.only(top:50.0, right: 10, left: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text("Please complete posting your advertisement by continue to our Checkout page."),
            _productSummary(),
            _chooseProduct(),
            _isSubmiting == false ? _checkout() : LinearProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Theme.of(context).accentColor),
            ),
        SizedBox(height: 20,),
        RaisedButton(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text("Cancel and Go Home", style: TextStyle(color: Colors.white),),
            ),
            color: Colors.red,
            onPressed: () => Navigator.pushNamed(context, '/home')

            ),
          ],
        )
      ),
    );
  }

  Widget _productSummary(){

    return Card(
      child: Text(this._advert.title),
    );

  }

  Widget _chooseProduct(){
     return Container(
       padding: EdgeInsets.all(10),
       child: Column(
         children: [
           RadioListTile(
               value: 1,
               groupValue: selectedRadioTile,
               title: Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                   Text("1 Month Advertisement"),
                 ],
               ),
               onChanged: (val) => setSelectedRadioTile(val),
             activeColor: Colors.green,
           ),
          /* RadioListTile(
               value: 2,
               groupValue: selectedRadioTile,
               title: Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                   Text("ADVERTISEMENT \$5 monthly"),
                   Text("Trial period - ad will also appear at ugalav.com", style: Theme.of(context).textTheme.caption,)
                 ],
               ),
               onChanged: (val) => setSelectedRadioTile(val),
             activeColor: Colors.blueAccent,
           ),*/
           SizedBox(
             height: 30,
           )
         ],
       ),
     );
  }

  Widget _checkout(){
    return RaisedButton(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Text("Continue", style: TextStyle(color: Colors.white, fontSize: 16.0),),
      ),
        color: Colors.blue,
        onPressed: () async {
          setState(() {
            _isSubmiting = true;
          });
          final sessionId = await fetchAdSessionId(this._advert.id.toString(), selectedRadioTile);
          setState(() {
            _isSubmiting = false;
          });
          Navigator.push(context, MaterialPageRoute(
            builder: (_) => AdvertStripePage(sessionId: sessionId,),
          ));
        });
  }

}
