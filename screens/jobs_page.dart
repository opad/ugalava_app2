import 'package:flutter/material.dart';
import 'package:ugalav/models/job.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:ugalav/widgets/FeaturedWidget.dart';
import 'package:ugalav/widgets/header_card.dart';
import 'package:ugalav/widgets/job_card.dart';

class ClassifiedsJobPage extends StatefulWidget {
  @override
  _ClassifiedsJobPageState createState() => _ClassifiedsJobPageState();
}

class _ClassifiedsJobPageState extends State<ClassifiedsJobPage> with AutomaticKeepAliveClientMixin<ClassifiedsJobPage>{

  @override
  bool get wantKeepAlive => true;
  List<Job> _jobs;
  ScrollController _controller = new ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getJobs();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => _refreshProducts(context),
      child: Container(
        color: Color.fromARGB(204, 204, 204, 204),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            HeaderCard('New Jobs'),
            _jobsList(_jobs),
            FeaturedWidget()
          ],
        )),
    );
  }

  Widget _jobsList(List<Job> jobs){
    if(jobs == null){
      return _loader();
    }
    if(jobs.length == 0){
      return Center(child: Text("No Classifieds Found"),);
    }
     return Expanded(
       child: ListView.builder(
           physics: const AlwaysScrollableScrollPhysics(), // new
           controller: _controller,
           itemCount: jobs.length,
           scrollDirection: Axis.vertical,
           shrinkWrap: true,
           itemBuilder: (context, index) {
             return Padding(
               padding: const EdgeInsets.only(left:4.0, right: 4.0),
               child: JobCard(jobs[index]),
             );
           }
       ),
     );
  }

  Widget _loader(){
    return Container(
      padding: EdgeInsets.all(30.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }

  void _getJobs() async{
    final List<Job> response = await fetchJobs();
    if(this.mounted) {
      setState(() {
        _jobs = response;
      });
    }
  }


  Future<void> _refreshProducts(BuildContext context) async {
    return await _getJobs();
  }

  @override
  void dispose() {
    super.dispose();
  }

}
