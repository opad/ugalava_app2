import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ugalav/utils/constants.dart' as constants;
import 'package:webview_flutter/webview_flutter.dart';


class AdvertStripePage extends StatefulWidget {
  final String sessionId;


  const AdvertStripePage({Key key, this.sessionId}) : super(key: key);

  @override
  _AdvertStripePageState createState() => _AdvertStripePageState();
}

class _AdvertStripePageState extends State<AdvertStripePage> {
  WebViewController _webViewController;

  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text("Payment"),
      ),
      body: WebView(
        initialUrl: initialUrl,
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (webViewController) =>
        _webViewController = webViewController,
        onPageFinished: (String url) {
          if (url == initialUrl) {
            _redirectToStripe(widget.sessionId);
          }
        },
        navigationDelegate: (NavigationRequest request) {
          if (request.url.startsWith(constants.BASE_APP_URL + '/advert_thanks')) {
            Navigator.of(context).pushReplacementNamed('/success-advert');
          } else if (request.url.startsWith(constants.BASE_APP_URL + '/advert_cancel')) {
            Navigator.of(context).pushReplacementNamed('/success-advert');
          }
          return NavigationDecision.navigate;
        },
      ),
    );
  }

  String get initialUrl =>
      'data:text/html;base64,${base64Encode(Utf8Encoder().convert(kStripeHtmlPage))}';

  Future<void> _redirectToStripe(String sessionId) async {
    final redirectToCheckoutJs = '''
var stripe = Stripe('pk_test_51HoBLJCYHZJHJezoU34CwKzSQVQjj6QfueGS1YiYjP4q4jgaET5VR24NLh7pxponhGykY0BAJX7xvHX02FWMXPim0041m0tqlR');
    
stripe.redirectToCheckout({
  sessionId: '$sessionId'
}).then(function (result) {
  result.error.message = 'Error'
});
''';

    try {
      await _webViewController.evaluateJavascript(redirectToCheckoutJs);
    } on PlatformException catch (e) {
      if (!e.details.contains(
          'JavaScript execution returned a result of an unsupported type')) {
        rethrow;
      }
    }
  }
}

const String kStripeHtmlPage = '''
<!DOCTYPE html>
<html>
<script src="https://js.stripe.com/v3/"></script>
<head><title>Stripe checkout</title></head>
<body>
<p style="padding-top: 200px;">
<center><h1>Loading... <h1/></center> 
</p>
</body>
</html>
''';