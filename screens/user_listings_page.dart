
import 'package:flutter/material.dart';
import 'package:ugalav/models/listings.dart';
import 'package:ugalav/screens/add_listing_page.dart';
import 'package:ugalav/screens/edit_listing_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class UserListings extends StatefulWidget {
  @override
  _UserListingsState createState() => _UserListingsState();
}

class _UserListingsState extends State<UserListings> {

  List<Listings> _listings;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getListings();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Businesses"),
      ),
      key: _scaffoldKey,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.push(context, MaterialPageRoute(
          builder: (_) => AddListingPage(),
        )),
      ),
      body: Container(
          padding: EdgeInsets.all(8.0),
          child: _listings == null ? Container(
            child: Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Theme
                    .of(context)
                    .accentColor),

              ),
            ),
          ) : ListView.separated(
              separatorBuilder: (context, index) => Divider(
                color: Colors.black,
              ),
              itemCount: _listings.length,
              itemBuilder: (_, index){

                return Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      caption: 'Edit',
                      color: Colors.black45,
                      icon: Icons.more_horiz,
                      onTap: () =>  Navigator.push(context, MaterialPageRoute(
                        builder: (_) => EditListingPage(item: _listings[index],),
                      )),
                    ),
                    IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () => _showSuccessSnack(_listings[index]),
                    ),
                  ],
                  child: ListTile(
                    //dense: true,
                    title: Text(_listings[index].title),
                    subtitle: Text("Created On:" + formatDate(_listings[index].created)),
                    /*trailing: Padding(
                      padding: const EdgeInsets.only(top:12.0),
                      child: Icon(Icons.edit, size: 18,),
                    ),*/
                    onTap: () => Navigator.push(context, MaterialPageRoute(
                      builder: (_) => EditListingPage(item: _listings[index],),
                    )).then((val)=> _getListings()),
                  ),
                );
              }
          )
      ),
    );
  }

  void _getListings() async{
    if(this.mounted){
      setState(() {
        _listings = null;
      });
    }
    final List<Listings> response = await fetchMyListing();
    setState(() {
      _listings = response;
    });
  }

  void _showSuccessSnack(Listings item) async {
    final snackbar = SnackBar(
        content: Text('Deleting ' + item.title, style: TextStyle(color: Colors.red))
    );
    _scaffoldKey.currentState.showSnackBar(snackbar);
    print("calling delete");
    var _isdeleted = await deleteListing(item.id);
     if(_isdeleted==204) {
         _listings.remove(item);
         final snackbar2 = SnackBar(
             content: Text(
                 'Deleted ' + item.title, style: TextStyle(color: Colors.red))
         );
         _scaffoldKey.currentState.showSnackBar(snackbar2);
         _getListings();
     }

  }
}
