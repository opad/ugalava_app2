import 'package:flutter/material.dart';
import 'package:ugalav/screens/add_classified_page.dart';
import 'package:ugalav/screens/add_listing_page.dart';
import 'package:ugalav/screens/jobs_page.dart';
import 'package:ugalav/screens/classifieds_page.dart';
import 'package:ugalav/screens/listings_page.dart';
import 'package:ugalav/screens/profile_page.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
    _tabController.addListener((){
      setState(() => _selectedIndex = _tabController.index);
      print("selected tabBar ${_tabController.index}");
    });

  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    precacheImage(new AssetImage('assets/images/icons/business_yellow.png'), context);
    precacheImage(new AssetImage('assets/images/icons/jobs_yellow.png'), context);
    precacheImage(new AssetImage('assets/images/icons/Profile_yellow.png'), context);
    precacheImage(new AssetImage('assets/images/icons/community_forum_white.png'), context);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<FloatingActionButton> fabs=[
      new FloatingActionButton(child: new Icon(Icons.post_add),
        onPressed: () => Navigator.push(context, MaterialPageRoute(
          builder: (_) => AddClassified(),
        )),),
      new FloatingActionButton(child: new Icon(Icons.add),onPressed: () => Navigator.push(context, MaterialPageRoute(
        builder: (_) => AddClassified(category: '3',),
      ))),
      new FloatingActionButton(child: new Icon(Icons.add_business),onPressed: () => Navigator.push(context, MaterialPageRoute(
        builder: (_) => AddListingPage(),
      )),)
    ];

    return DefaultTabController(
      length: 4,
      child: Scaffold(
          floatingActionButton: _selectedIndex < 3 ? fabs[_selectedIndex] : null,
          appBar: AppBar(
            toolbarHeight: 60,
            backgroundColor: Theme.of(context).primaryColor,
            bottom: TabBar(
              controller: _tabController,
              indicatorColor: Colors.yellow,
                indicatorPadding: EdgeInsets.only(top: 5.0),
                unselectedLabelColor: Colors.yellow,
              onTap: (index) => {
                setState(() => _selectedIndex = index)
              },
              tabs: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom:8.0),
                  child: Tab(child: _selectedIndex ==0 ? Image.asset("assets/images/icons/community_forum_yellow.png") : Image.asset("assets/images/icons/community_forum_white.png") ,),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom:8.0),
                  child: Tab(icon: _selectedIndex ==1 ? Image.asset("assets/images/icons/jobs_yellow.png"):Image.asset("assets/images/icons/jobs_white.png"),),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom:8.0),
                  child: Tab(icon: _selectedIndex ==2 ? Image.asset("assets/images/icons/business_yellow.png"):Image.asset("assets/images/icons/business_white.png"),),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom:8.0),
                  child: Tab(icon: _selectedIndex ==3 ? Image.asset("assets/images/icons/Profile_yellow.png"):Image.asset("assets/images/icons/Profile_white.png"),),
                ),
              ],
            ),
           // title: Text('North American Uganda Community', style: TextStyle(fontSize: 14.0,)),
          ),
        body: TabBarView(
          controller: _tabController,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            ClassifiedsPage(),
            ClassifiedsJobPage(),
            ListingScreen(),
            ProfilePage()
          ],
        ),
      ),
    );
  }
}
