import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ugalav/utils/constants.dart' as constants;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:google_sign_in/google_sign_in.dart';


GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);


class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final _formKey = GlobalKey <FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  GoogleSignInAccount _currentUser;
  bool _isSubmitting = false;
  bool _obscureText = true;
  String _username, _password;
  @override
  void initState() {
    super.initState();
  }

  Widget _showTitle() {
    return Column(
      children: <Widget>[
        Image(image: AssetImage('assets/images/logo_small.png'),),
        SizedBox(height: 10.0,),
        Text("serving", style: TextStyle(color: Colors.white)),
        Padding(
          padding: const EdgeInsets.only(top:5.0, bottom: 10.0),
          child: Text('North American Ugandan Community', style: TextStyle(color: Colors.white),),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('Log In', style: Theme.of(context).textTheme.title.copyWith(color: Colors.white)),
        ),
      ],
    );
  }




  Widget _showUsernameInput() {
    return Padding(
        padding: EdgeInsets.only(top: 5.0),
        child: TextFormField(
            onSaved: (val) => _username = val,
            //keyboardType: TextInputType.phone,
            validator: (val) => val.length < 3 ? 'Invalid Username': null,
            cursorColor: Colors.white,
            decoration: InputDecoration(
              //contentPadding: EdgeInsets.all(4.0),
                filled: true,
                border: OutlineInputBorder(),
                fillColor: Colors.white,
                labelText: 'Username',
                hintText: 'Enter Your Username',
                )));
  }

  Widget _showPasswordInput() {
    return Padding(
        padding: EdgeInsets.only(top: 5.0),
        child: TextFormField(
            onSaved: (val) => _password = val,
            validator: (val) => val.length < 6 ? 'Password too short': null,
            obscureText: _obscureText,
            decoration: InputDecoration(
                suffixIcon: GestureDetector(
                  onTap: (){
                    setState(() => _obscureText = !_obscureText);
                  },
                  child: Icon(_obscureText ? Icons.visibility: Icons.visibility_off),
                ),
                border: OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
                labelText: 'Password',
                hintText: 'Enter password, min length 6',
            )));
  }

  Widget _showFormActions() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _isSubmitting == true ? Container(
                width: 50,
                child: LinearProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Theme.of(context).accentColor),
                ) ,
              ): RaisedButton(
                  padding: EdgeInsets.all(15.0),
                  child: Text('Log In',
                      style: Theme.of(context)
                          .textTheme
                          .body1
                          .copyWith(color: Colors.white)),
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  color: Colors.red,
                  onPressed: _submit),
               SizedBox(height: 20,),
               Divider(
                 height: 1,
               )
            /*  FlatButton(
                  padding: EdgeInsets.all(20),
                  child: Text('New user? Join Us', style: TextStyle(decoration: TextDecoration.underline,),),
                  onPressed: () => Navigator.pushReplacementNamed(context, '/register'))*/
            ]));
  }

  void _submit(){
    final form = _formKey.currentState;
    if(form.validate()){
      form.save();
      _loginUser();
    }
  }

  _loginFacebook() async {
    final facebookLogin = FacebookLogin();
    //facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
    final result = await facebookLogin.logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        print(result.accessToken.token);
        _convertToken(result.accessToken.token, 'facebook');
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("user canceled");
        break;
      case FacebookLoginStatus.error:
        print("An Error Occurred");
        break;
    }

  }

  _signInWithGoogle() async {
    final GoogleSignIn _googleSignin = new GoogleSignIn(scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],);

    try {
      GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();

      GoogleSignInAuthentication googleSignInAuthentication =  await googleSignInAccount.authentication;
      print(googleSignInAuthentication.idToken);
    } catch (e) {
      print(e.toString());
    }


  }

  _convertToken(String providerToken, String provider) async {
    setState(() => _isSubmitting = true);
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Accept': 'application/json',
    };
   http.Response response = await http.post(constants.BASE_APP_URL + '/auth/convert-token',
        body: {
          "grant_type": 'convert_token',
          "client_id": 'PyAoZWYqPN0FAhPSLUKSmhJXe64xbaccyazoMbN6',
          "client_secret":'9cyKxRnH2v8fycyvfcdFQo4EvdgYmO72OKbHQaf93spphaDmeWPfjXYzN82K6MaWLmyJMKw7MDQVWZMaHIQtp0HUNaDoy7MPkqFXqwijzxlpn09ineNOWwDOoSbNG2t0',
          "backend": provider,
          "token": providerToken
        }
    );

    final responseData = json.decode(response.body);
    print(responseData);
    if(response.statusCode == 200){
      setState(() => _isSubmitting = false);
      print(responseData);
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('jwt', responseData['access_token']);
      prefs.setString('jwt_provider', provider);
      Navigator.pushReplacementNamed(context, '/home');

    }else{
      setState(() => _isSubmitting = false);
      String errorMsg = responseData['message'];
      _showErrorSnack(errorMsg);
    }
  }

  void _loginUser() async {
    setState(() => _isSubmitting = true);
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Accept': 'application/json',
    };
  /*  http.Response response = await http.post(constants.BASE_URL + '/token/',
        body: json.encode({
          "username": _username,
          "password": _password
        }), headers: headers
    );*/
    http.Response response = await http.post(constants.BASE_APP_URL + '/auth/token',
        body: {
          "grant_type": 'password',
          "client_id": 'VTafoi1MxF6TLwavB15iWyi2yhzcs3Mjcd5xdpNO',
          "client_secret": "7M3aYg5BjLkuo37TpoZk9jzaxwiXsZL2LIlpIYwbrgCWLgsn6W9pEQkndkQJlOex5yD5x7XlDIm3Hf5sb5FjqknYEgX2p4YW2ta8DypyuG9pVuK8YX2r2uRrlPVjLi1O",
          "username": _username,
          "password": _password
        }
    );
    final responseData = json.decode(response.body);
    print(response.statusCode);
    print(response.body);
    if(response.statusCode == 200){
      setState(() => _isSubmitting = false);
      print(responseData);
      _storeUserData(responseData);
      _showSuccessSnack();
      _redirectUser();

    }else if(response.statusCode == 400){
      setState(() => _isSubmitting = false);
      if(responseData.containsKey('error_description')){
         _showErrorSnack(responseData['error_description']);
      }else{
        _showErrorSnack("Invalid Username or Password");
      }

    }else{
      setState(() => _isSubmitting = false);
      String errorMsg = responseData['detail'];
      _showErrorSnack(errorMsg);
    }


  }

  void _storeUserData(responseData) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('jwt',responseData['access_token']);
  }

  void _showSuccessSnack(){
    final snackbar = SnackBar(
        content: Text('User successfully Logged In', style: TextStyle(color: Colors.green))
    );

    _scaffoldKey.currentState.showSnackBar(snackbar);
    _formKey.currentState.reset();
  }

  void _showErrorSnack(String errorMsg){
    final snackbar = SnackBar(
        content: Text(errorMsg, style: TextStyle(color: Colors.red))
    );

    _scaffoldKey.currentState.showSnackBar(snackbar);
    throw Exception('Error Loggin: $errorMsg');
  }

  void _redirectUser(){
    Future.delayed(Duration(seconds: 2), () {
      Navigator.pushReplacementNamed(context, '/home');

    });
  }

  Widget _loginForm(){
    return Form(
        key: _formKey,
        child: Column(children: [
          _showTitle(),
          _showUsernameInput(),
          _showPasswordInput(),
          _showFormActions()
        ]));
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: [
                _loginForm(),
                _socialLogin(),
                FlatButton(
                    padding: EdgeInsets.all(20),
                    child: Text('Register with Us', style: TextStyle(decoration: TextDecoration.underline,),),
                    onPressed: () => Navigator.pushReplacementNamed(context, '/register'))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _loader(){
    return Container(
      padding: EdgeInsets.all(30.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }

  Widget _socialLogin(){
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text("OR"),
        ),
        FacebookSignInButton(onPressed: () {
          _loginFacebook();
        }, text: "Login with Facebook", textStyle: TextStyle(fontWeight: FontWeight.normal, color: Colors.white),),
        GoogleSignInButton(onPressed: () {
          _signInWithGoogle();
        }, text: "Login with Google", textStyle: TextStyle(fontWeight: FontWeight.normal, color: Colors.black54),)
      ],
    );
  }
}
