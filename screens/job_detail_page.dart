
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:ugalav/models/job.dart';
import 'package:url_launcher/url_launcher.dart';

class JobDetailPage extends StatefulWidget {
  final Job item;
  JobDetailPage({Key key, @required this.item}):super(key: key);

  @override
  _JobDetailPageState createState() => _JobDetailPageState(item);
}

class _JobDetailPageState extends State<JobDetailPage> {
  final Job classified;

  _JobDetailPageState(this.classified);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Classified Details"),
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Hero(
                  tag: 'photo' + classified.id.toString(),
                    child: Image(image: _photo(),)
                ),
                _classifiedDetails(),
              ],
            ),
          ),
        ),
    );
  }

  Widget _classifiedDetails(){
    var rate_period = classified.rate_period != null ? '/' + classified.rate_period: '';
     return Container(
       padding: EdgeInsets.all(8.0),
       child: Column(
         crossAxisAlignment: CrossAxisAlignment.start,
         children: [
           Text(classified.title, style: Theme.of(context).textTheme.title.copyWith(fontWeight: FontWeight.bold)),
           Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: [
               Text('\$' + classified.price + '' + rate_period, style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold)),

               RaisedButton.icon(
                 icon: Icon(Icons.phone),
                 label: Text("Call"),
                   color: Theme.of(context).accentColor,
                   onPressed: () => _launchURL('tel:' + classified.phone)
               ),
             ],
           ),
           SizedBox(height: 20,),
           Text(classified.detail),
           Center(child: _shareBox()),
         ],
       ),
     );
  }

  Widget _shareBox(){
    var phone = classified.phone ?? 'No Number';
    String body = 'Ugalav classified: ' + classified.title + '\n' + phone;
    return RaisedButton(
        child: Text("Share"),
        onPressed: () => {
          Share.share(body, subject:'Ugalav Shared a classified')
        });
  }

  void _launchURL(String url) async {

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print('Could not launch $url');
    }
  }

  ImageProvider _photo(){
    return classified.photo == null ? AssetImage('assets/images/no-image-icon.jpg') : NetworkImage(classified.photo);
  }

}
