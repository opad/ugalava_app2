
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:ugalav/models/advert.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/screens/classified_checkout_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:url_launcher/url_launcher.dart';

class AdvertDetailPage extends StatefulWidget {
  final Advert item;
  AdvertDetailPage({Key key, @required this.item}):super(key: key);

  @override
  _AdvertDetailPageState createState() => _AdvertDetailPageState(item);
}

class _AdvertDetailPageState extends State<AdvertDetailPage> {
  final Advert item;

  _AdvertDetailPageState(this.item);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Advert detail"),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(item.title, style: Theme.of(context).textTheme.title.copyWith(fontWeight: FontWeight.bold)),
                Hero(
                  tag: 'photo' + item.id.toString(),
                    child: Image(image: _photo(),)
                ),
                _card_text(context),
                SizedBox(height: 20,),
                SizedBox(height: 20,),
                _shareBox(),

              ],
            ),
          ),
        ),
    );
  }


  Widget _card_text(BuildContext context){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10,),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Icon(Icons.phone, size: 15, color: Colors.grey,),
              ),
              InkWell(
                onTap: () => _launchURL('tel:' + this.item.phone),
                child: Text(this.item.phone ?? 'No Contact', style: Theme.of(context).
                textTheme.subtitle2.copyWith(color: Colors.red, fontWeight: FontWeight.bold)),
              ),

            ],
          ),
          SizedBox(height: 10,),
          InkWell(
            onTap: () => _launchURL(this.item.url),
            child: Text("Visit website", style: Theme.of(context).
            textTheme.subtitle2.copyWith(color: Colors.blue, fontWeight: FontWeight.bold)),
          ),
        ],
      ),
    );
  }

  Widget _shareBox(){
    var phone = item.phone ?? 'No Number';
    String body = 'Ugalav classified: ' + item.title + '\n' + phone;
    return RaisedButton(
        child: Text("Share"),
        onPressed: () => {
          Share.share(body, subject:'Ugalav Shared a classified')
        });
  }

  void _launchURL(String url) async {

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print('Could not launch $url');
    }
  }

  ImageProvider _photo(){
    return item.photo == null ? AssetImage('assets/images/no-image-icon.jpg') : NetworkImage(item.photo);
  }

}
