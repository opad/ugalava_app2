
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:ugalav/models/advert.dart';
import 'package:ugalav/screens/advert_checkout_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:ugalav/utils/constants.dart' as constants;
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddAdvert extends StatefulWidget {
  @override
  _AddAdvertState createState() => _AddAdvertState();
}

class _AddAdvertState extends State<AddAdvert> {

  final _formKey = GlobalKey <FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _isSubmitting = false;
  File _image;
  Advert _advert;
  String _title,  _phone, _url, _photo;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
       appBar: AppBar(
         title: Text("Post to Advert"),
       ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              this._isSubmitting ? _loader() : _FormFields()
            ],
          ),
        ),
      ),
    );
  }

  Widget _loader(){
    return Container(
      padding: EdgeInsets.all(30.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }

  Widget _FormFields(){
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
            children: [
          Text("Title", style: TextStyle(fontWeight: FontWeight.bold),),
          _showTitleInput(),
          SizedBox(height: 5,),
          Text("Contact Phone", style: TextStyle(fontWeight: FontWeight.bold),),
          _showPhoneInput(),
              SizedBox(height: 5,),
          SizedBox(height: 5,),
          Text("Website URL", style: TextStyle(fontWeight: FontWeight.bold),),
          _showURLInput(),
          SizedBox(height: 5,),
          _imagePicker(),
          _showFormActions()
        ]));
  }

  Widget _showTitleInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _title = val,
            validator: (val) => val.length < 9 ? 'Invalid Title': null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Title',
              hintText: 'Enter Advert Title',
            )));
  }

  Widget _showURLInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _url = val,
            validator: (val){
              bool _validURL = Uri.parse(val).isAbsolute;
              if(_validURL){
                return null;
              }
              return "Please enter Invalid Url starting http";
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Price',
              hintText: 'Enter website starting http',
            )));
  }

  Widget _showPhoneInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _phone = val,
            keyboardType: TextInputType.phone,
            validator: (val) => val.length != 10 ? 'Invalid Phone Number': null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Phone',
              hintText: 'Enter a Contact Phone Number',
            )));
  }


  Widget _showFormActions() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _isSubmitting == true ? Container(
                width: 50,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Theme
                      .of(context)
                      .accentColor),

                ),
              ) : RaisedButton(
                  padding: EdgeInsets.all(15.0),
                  child: Text('Continue',
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .copyWith(color: Colors.white)),
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  color: Theme
                      .of(context)
                      .accentColor,
                  onPressed: _submit),

            ]));
  }

  Widget _imagePicker(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          _image == null ? Text('No image selected.'): SizedBox(child:Image.file(_image) , width: 100, height: 80,),
          FlatButton(onPressed: () => _showPicker(context),
              child: Text("Select Photo")),
        ],
      ),
    );
  }

  void _submit() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {
        _isSubmitting = true;
      });

       saveAndUpload().then((httpCode) =>
       {
         if(httpCode == 200 || httpCode == 201){
                _popAndCheckout()

         }else if(httpCode == 401){
           _errorMessage("Your Session has Expired! \n Please login again")
          }else{
            _errorMessage("An Error has Occurred \nplease try again later.")
         }
       });
    }
  }

  _errorMessage(String msg){
    SweetAlert.show(context,
        title: "Ops",
        subtitle: msg,
        style: SweetAlertStyle.error,
        onPress: (bool choice){
          Navigator.pop(context);
          return true;
        });
      setState(() {
        _isSubmitting = false;
      });
  }

  _popAndCheckout(){
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(
        builder: (_) => AdvertCheckout(item: this._advert)
    ));
  }

  Future<int> saveAndUpload() async {

    var url = constants.BASE_URL + '/adverts/';
    var filename = _photo;
    final prefs = await SharedPreferences.getInstance();
    String storedUser = prefs.getString('jwt');
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + storedUser
    };
    var request = http.MultipartRequest('POST', Uri.parse(url),);
    request.headers.addAll(headers);
    if(filename !=null){
      request.files.add(await http.MultipartFile.fromPath('photo', filename));
    }
    request.fields['title'] = this._title;
    request.fields['phone'] = this._phone;
    request.fields['url'] = this._url;


    var res = await request.send();
    var response = await http.Response.fromStream(res);
    if(res.statusCode ==201){
      var jsonData = jsonDecode(response.body);
       this._advert = Advert.fromJson(jsonData);
    }
    return res.statusCode;
  }

  void _getImage(ImageSource imageSource) async {
    var file = await ImagePicker().getImage(source: imageSource, maxWidth: 800);
    if(file !=null) {
      setState(() {
        _image = File(file.path);
        _photo = file.path;
      });
    }
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _getImage(ImageSource.gallery);
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _getImage(ImageSource.camera);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
}
