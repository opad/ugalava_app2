
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:ugalav/models/categories.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/screens/classified_checkout_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:ugalav/widgets/header.dart';
import 'package:ugalav/utils/constants.dart' as constants;
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditClassified extends StatefulWidget {
  final Classified classified;
  EditClassified({Key key, this.classified}):super(key: key);
  @override
  _EditClassifiedState createState() => _EditClassifiedState(classified);
}

class _EditClassifiedState extends State<EditClassified> {
  Classified _item;
  _EditClassifiedState(this._item);

  final _formKey = GlobalKey <FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _isSubmitting = false;
  Classified _classified;
  File _image;
  List<DropdownMenuItem<String>> _categories = [];
  String _cat;
  String _title, _details, _phone, _price, _negotiable, _seller, _photo;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCategories();
  }

  _getCategories() async {
     List<Category> categories = await fetchCategories();
     List<DropdownMenuItem<String>> catList = [];
     for(var i = 0; i < categories.length; i++){
       catList.add(
           DropdownMenuItem<
               String>(
             value: categories[i].id.toString(),
             child: Text(categories[i].name),
           )
       );
     }

     setState(() {
       _categories = catList;
     });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
       appBar: AppBar(
         title: Text("Edit Classified"),
       ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              //Header("Classified Registration Form"),
              this._isSubmitting ? _loader() : _FormFields()
            ],
          ),
        ),
      ),
    );
  }

  Widget _loader(){
    return Container(
      padding: EdgeInsets.all(30.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }

  Widget _FormFields(){
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
            children: [
          Text("Title", style: TextStyle(fontWeight: FontWeight.bold),),
          _showTitleInput(),
          SizedBox(height: 5,),
          Text("Contact Phone", style: TextStyle(fontWeight: FontWeight.bold),),
          _showPhoneInput(),
              SizedBox(height: 5,),
              Text("Category", style: TextStyle(fontWeight: FontWeight.bold),),
              _showCategoryInput(),
          SizedBox(height: 5,),
          Text("Price", style: TextStyle(fontWeight: FontWeight.bold),),
          _showPriceInput(),
          SizedBox(height: 5,),
          _imagePicker(),
          _showBriefInput(),

          _showFormActions()
        ]));
  }

  Widget _showTitleInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _item.title = val,
            initialValue: _item.title,
            //validator: (val) => val.length < 9 ? 'Invalid Phone Number': null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Title',
              hintText: 'Enter Classified Title',
            )));
  }

  Widget _showPriceInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _item.price = val,
            initialValue: _item.price,
            //validator: (val) => val.length < 9 ? 'Invalid Phone Number': null,
            decoration: InputDecoration(
              prefixText: ' \$ ',
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Price',
              hintText: 'Enter item Price',
            )));
  }

  Widget _showPhoneInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _item.phone = val,
            initialValue: _item.phone,
            keyboardType: TextInputType.phone,
            //validator: (val) => val.length < 9 ? 'Invalid Phone Number': null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(2.0),
              border: OutlineInputBorder(),
              //labelText: 'Phone',
              hintText: 'Enter a Contact Phone Number',
            )));
  }


  Widget _showBriefInput() {
    return Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: TextFormField(
            onSaved: (val) => _item.detail = val,
            initialValue: _item.detail,
            maxLines: 5,
            maxLength: 1000,
            //validator: (val) => val.length < 3 ? 'Invalid Address' : null,
            decoration: InputDecoration(

              border: OutlineInputBorder(),
              hintText: 'Tell Something about your Business',

            )));
  }

  Widget _showCategoryInput() {
    return DropdownButtonFormField <String>(
      value: _item.category.toString(),
      isExpanded: true,
      items: _categories,
      //hint: Text('City'),
      onChanged: (String newValue) {
        setState(() {
          _item.category = int.parse(newValue);
        });
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        contentPadding: EdgeInsets.all(2.0),
        hintText: 'Category',
      ),

    );
  }

  Widget _showFormActions() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _isSubmitting == true ? Container(
                width: 50,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Theme
                      .of(context)
                      .accentColor),

                ),
              ) : RaisedButton(
                  padding: EdgeInsets.all(15.0),
                  child: Text('Save Forum Post',
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .copyWith(color: Colors.white)),
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  color: Theme
                      .of(context)
                      .accentColor,
                  onPressed: _submit),

            ]));
  }

  Widget _imagePicker(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          _image == null ? Text('No image selected.'): SizedBox(child:Image.file(_image) , width: 100, height: 80,),
          FlatButton(onPressed: () => _showPicker(context),
              child: Text("Select Photo")),
        ],
      ),
    );
  }

  void _submit() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {
        _isSubmitting = true;
      });

       saveAndUpload().then((httpCode) =>
       {
         if(httpCode == 200 || httpCode == 201){
            _showList()
         }else if(httpCode == 401){
           _errorMessage("Your Session has Expired! \n Please login again")
          }else{
            _errorMessage("An Error has Occurred \nplease try again later.")
         }
       });
    }
  }

  _showList(){
    setState(() {
      _isSubmitting = false;
    });
    SweetAlert.show(context,
        title: "Success!",
        subtitle: "Your Post has been updated!",
        style: SweetAlertStyle.success,
        onPress: (bool choice){
          Navigator.pop(context);
          return true;
        });
  }

  _errorMessage(String msg){
    SweetAlert.show(context,
        title: "Ops",
        subtitle: msg,
        style: SweetAlertStyle.error,
        onPress: (bool choice){
          Navigator.pop(context);
          return true;
        });
      setState(() {
        _isSubmitting = false;
      });
  }

  Future<int> saveAndUpload() async {

    var url = constants.BASE_URL + '/user/classifieds/' + _item.id.toString() +'/';
    var filename = _photo;
    final prefs = await SharedPreferences.getInstance();
    String storedUser = prefs.getString('jwt');
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + storedUser
    };
    var request = http.MultipartRequest('PUT', Uri.parse(url),);
    request.headers.addAll(headers);
    if(filename !=null){
      request.files.add(await http.MultipartFile.fromPath('photo', filename));
    }
    request.fields['title'] = _item.title;
    request.fields['phone'] = _item.phone;
    request.fields['expected_price'] = _item.price;
    request.fields['detail'] = _item.detail;
    request.fields['is_price_negotiable'] = 'Y';
    request.fields['is_seller'] = 'Y';
    request.fields['is_individual'] = 'Y';
    //request.fields['is_active'] = 'Y';
    request.fields['category'] = _item.category.toString();
    print(request.fields);
    var res = await request.send();
    var response = await http.Response.fromStream(res);
    if(response.statusCode ==201){
      var jsonData = jsonDecode(response.body);
       this._classified = Classified.fromJson(jsonData);
    }
    return res.statusCode;
  }

  void _getImage(ImageSource imageSource) async {
    var file = await ImagePicker().getImage(source: imageSource, maxWidth: 800);
    if(file !=null) {
      setState(() {
        _image = File(file.path);
        _photo = file.path;
      });
    }
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _getImage(ImageSource.gallery);
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _getImage(ImageSource.camera);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
}
