
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/models/listings.dart';
import 'package:ugalav/screens/add_classified_page.dart';
import 'package:ugalav/screens/edit_classified_page.dart';
import 'package:ugalav/screens/edit_listing_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';

class UserForum extends StatefulWidget {
  @override
  _UserForumState createState() => _UserForumState();
}

class _UserForumState extends State<UserForum> {

  List<Classified> _items;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getClassified();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("My Forum Posts"),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.push(context, MaterialPageRoute(
          builder: (_) => AddClassified(),
        )),
      ),
      body: Container(
        padding: EdgeInsets.all(8.0),
          child: _items == null ? Container(
            child: Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Theme
                    .of(context)
                    .accentColor),
              ),
            ),
          ) : ListView.separated(
              separatorBuilder: (context, index) => Divider(
                color: Colors.black,
              ),
              itemCount: _items.length,
              itemBuilder: (_, index){

                return Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      caption: 'Edit',
                      color: Colors.black45,
                      icon: Icons.more_horiz,
                      onTap: () =>  Navigator.push(context, MaterialPageRoute(
                        builder: (_) => EditClassified(classified: _items[index],),
                      )),
                    ),
                    IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () => _showSuccessSnack(_items[index]),
                    ),
                  ],
                  child: ListTile(
                    //dense: true,
                    title: Text(_items[index].title),
                    subtitle: Text(formatDate(_items[index].created)),
                    /*trailing: Padding(
                      padding: const EdgeInsets.only(top:12.0),
                      child: Icon(Icons.edit, size: 18,),
                    ),*/
                    onTap: () => Navigator.push(context, MaterialPageRoute(
                      builder: (_) => EditClassified(classified: _items[index],),
                    )).then((val)=> _getClassified()),
                  ),
                );
              }
          )
      ),
    );
  }

  void _getClassified() async{
    final List<Classified> response = await fetchMyClassifieds();
    if(this.mounted) {
      setState(() {
        _items = response;
      });
    }
  }

  void _showSuccessSnack(Classified item) async {
    final snackbar = SnackBar(
        content: Text('Deleting ' + item.title, style: TextStyle(color: Colors.red))
    );
    _scaffoldKey.currentState.showSnackBar(snackbar);
    print("calling delete");
    var _isdeleted = await deleteClassified(item.id);
    if(_isdeleted==204) {
      final snackbar2 = SnackBar(
          content: Text(
              'Deleted ' + item.title, style: TextStyle(color: Colors.red))
      );
      _scaffoldKey.currentState.showSnackBar(snackbar2);
      _getClassified();
    }

  }
}
