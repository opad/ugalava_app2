
import 'package:flutter/material.dart';
import 'package:ugalav/models/listings.dart';
import 'package:ugalav/utils/utility_funcs.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class ListingDetailPage extends StatefulWidget {
  final Listings item;

  ListingDetailPage({Key key, @required this.item}): super(key: key);

  @override
  _ListingDetailPageState createState() => _ListingDetailPageState(item);
}

class _ListingDetailPageState extends State<ListingDetailPage> {
  final Listings listing;

  _ListingDetailPageState(this.listing);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Listing Details'),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Hero(
                  tag: 'listing' + listing.id.toString(),
                  child: Text(listing.title, style: Theme.of(context).textTheme.title.copyWith(fontWeight: FontWeight.bold))
              ),

              SizedBox(height: 10.0,),
              Text('Joined: ' + formatDate(listing.created), style: TextStyle(color: Colors.green),),
              SizedBox(height: 10.0,),
              Row(
                children: <Widget>[
                  Icon(Icons.phone, color: Colors.grey, size: 17.0,),
                  Padding(
                    padding: const EdgeInsets.only(left: 3.0),
                    child: InkWell(
                      child: Text(
                          listing.phone,
                          style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.red)),
                      onTap: () => _launchURL('tel:' + listing.phone),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8.0,),
              Divider(),
              _addressWidget(),
              _physicalAddress(),
              SizedBox(height: 8.0,),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.home_work_outlined),
                  ),
                  InkWell(
                    child: Text(
                        'Visit Website',
                        style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.blue)),
                    onTap: () => _launchURL(listing.website),
                  ),
                ],
              ),
              Divider(),
              Text(listing.body),
              SizedBox(height: 10,),
              _shareBox()
            ],
          ),
        ),
      ),
    );
  }

  Widget _shareBox(){
    String body = 'Ugalav Listing: ' + listing.title + '\n' + listing.phone;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        RaisedButton(
           child: Text("Share"),
            onPressed: () => {
            Share.share(body, subject:'Ugalav Shared a Listing')
            }),
        RaisedButton(
            child: Text("Send Mail"),
            onPressed: () => {
              _launchURL('mailto:' + listing.email)
            }),
      ],
    );
  }

  Widget _physicalAddress(){
     var apt = listing.apt ?? 'no info';
      return Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.business),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Apt:" + apt),
                Text("house: " + listing.house)
              ],
          ),
        ],
      );
  }

  Widget _addressWidget(){
    var zip = listing.zip ?? '';
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(child: Icon(Icons.location_on), onTap: () => _launchURL(
    'https://www.google.com/maps/search/?api=1&query=' + listing.lat +',' + listing.longt))),
          Expanded(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(listing.contact),
                Text(listing.street + ' ' +listing.city , overflow: TextOverflow.ellipsis,
                    maxLines: 2,),
                Text(listing.state + ',' + zip ),

              ],
            ),
          ),
          Spacer(),
          InkWell(child: Icon(Icons.map), 
              onTap: () => _launchURL(
              'https://www.google.com/maps/search/?api=1&query=' + listing.lat +',' + listing.longt))
        ],
      );
  }

  void _launchURL(String url) async {

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
