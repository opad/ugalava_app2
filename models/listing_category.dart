
import 'package:meta/meta.dart';

class ListingCategory{
   int id;
   String name;

   ListingCategory({
     @required this.id,
     @required this.name
});

   factory ListingCategory.fromJson(Map<String, dynamic> json){
      return ListingCategory(
          id: json['id'],
          name: json['name']
      );
   }
}