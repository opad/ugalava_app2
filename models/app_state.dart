import 'package:meta/meta.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/models/classifieds.dart';
import 'package:ugalav/models/job.dart';
import 'package:ugalav/models/listings.dart';


@immutable
class AppState{
  final List<Listings> listings;
  final List<Classified> classifieds;
  final List<Listings> myListings;
  final List<Job> jobs;

  AppState({
      @required this.listings,
      @required this.classifieds,
      @required this.myListings,
      @required this.jobs
  });

  factory AppState.initial(){
    return AppState(
        listings: [],
        classifieds: [],
        myListings: [],
        jobs: []
    );
  }
}