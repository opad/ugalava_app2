import 'package:meta/meta.dart';

class Classified{
  int id;
  String title;
  String detail;
  String photo;
  int category;
  String phone;
  String price;
  String is_negotiable;
  String rate_period;
  String owner;
  String created;

  Classified({
    @required this.id,
    @required this.title,
    @required this.detail,
    @required this.photo,
    @required this.category,
    @required this.phone,
    @required this.price,
    @required this.is_negotiable,
    @required this.rate_period,
    @required this.owner,
    @required this.created
  });

  factory Classified.fromJson(Map<String, dynamic> json){
    return Classified(
        id: json['id'],
        title: json['title'],
        detail: json['detail'],
        photo: json['photo'],
        category: json['category'],
        phone: json['phone'],
        price: json['expected_price'],
        is_negotiable: json['is_price_negotiable'],
        rate_period: json['rate_period'],
        owner: json['owner'],
        created: json['created']
    );
  }
}