import 'package:meta/meta.dart';

class Job{
  int id;
  String title;
  String detail;
  String phone;
  String price;
  String photo;
  String is_negotiable;
  String rate_period;
  String owner;
  String created;

  Job({
    @required this.id,
    @required this.title,
    @required this.detail,
    @required this.phone,
    @required this.price,
    @required this.photo,
    @required this.is_negotiable,
    @required this.rate_period,
    @required this.owner,
    @required this.created
  });

  factory Job.fromJson(Map<String, dynamic> json){
    return Job(
        id: json['id'],
        title: json['title'],
        detail: json['detail'],
        phone: json['phone'],
        price: json['expected_price'],
        photo: json['photo'],
        is_negotiable: json['is_price_negotiable'],
        rate_period: json['rate_period'],
        owner: json['owner'],
        created: json['created']
    );
  }
}