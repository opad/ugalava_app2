import 'package:meta/meta.dart';

class Advert{
   int id;
   String next;
   String previous;
   String title;
   String photo;
   String phone;
   String url;

   Advert({
     @required this.id,
     @required this.title,
     @required this.photo,
     @required this.phone,
     @required this.url
  });

   factory Advert.fromJson(Map<String, dynamic> json){
      return Advert(
          id: json['id'],
          title: json['title'],
          photo: json['photo'],
          phone: json['phone'],
          url: json['url']
      );
   }

   set prev_url(String name){
      this.previous = name;
   }
   set next_url (String name){
     this.next = name;
   }
}