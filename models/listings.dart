import 'package:meta/meta.dart';

class Listings{
  int id;
  String title;
  String category;
  String contact;
  String phone;
  String website;
  String email;
  String city;
  String house;
  String street;
  String state;
  String zip;
  String apt;
  String body;
  String lat;
  String longt;
  String created;

  Listings({
    @required this.id,
    @required this.title,
    @required this.category,
    @required this.contact,
    @required this.phone,
    @required this.website,
    @required this.email,
    @required this.city,
    @required this.house,
    @required this.street,
    @required this.state,
    @required this.zip,
    @required this.apt,
    @required this.body,
    @required this.lat,
    @required this.longt,
    @required this.created
  });

  factory Listings.fromJson(Map<String, dynamic> json){
     return Listings(
       id: json['id'],
       title: json['title'],
       category: json['category'],
       contact: json['contact'],
       phone: json['phone'],
       website: json['website'],
       email: json['email'],
       city: json['city'],
       house: json['house'],
       street: json['street'],
       state: json['state'],
       zip: json['zipcode'],
       apt: json['apt_no'],
       body: json['body'],
       lat: json['lat'],
       longt: json['longt'],
       created: json['created']
     );
  }
}