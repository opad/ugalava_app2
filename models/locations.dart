
import 'package:meta/meta.dart';

class Location{
   int id;
   String name;

   Location({
     @required this.id,
     @required this.name
});

   factory Location.fromJson(Map<String, dynamic> json){
      return Location(
          id: json['id'],
          name: json['name']
      );
   }
}