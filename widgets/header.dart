
import 'package:flutter/material.dart';
import 'package:ugalav/screens/add_classified_page.dart';

class Header extends StatelessWidget {
  final String title;
  Header(this.title);
  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(this.title.toUpperCase()),
            ],
          ),
        )
    );
  }
}
