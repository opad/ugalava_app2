
import 'package:flutter/material.dart';
import 'package:ugalav/screens/add_classified_page.dart';

class HeaderCard extends StatelessWidget {
  final String title;
  HeaderCard(this.title);
  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(this.title.toUpperCase()),
              InkWell(child: Icon(Icons.more_vert), onTap: () =>Navigator.push(context, MaterialPageRoute(
                  builder: (_) => AddClassified()
              )),)
            ],
          ),
        )
    );
  }
}
