
import 'package:flutter/material.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/models/classifieds.dart';
import 'package:ugalav/screens/classified_detail_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';

class ClassifiedCard extends StatelessWidget {
  final Classified classified;
  ClassifiedCard(this.classified);
  @override
  Widget build(BuildContext context) {
    return Card(
      //margin: EdgeInsets.only(top: 30),
      child: ListTile(
        isThreeLine: true,
        //dense: true,
        leading: this.classified.photo == null ? Image.asset('assets/images/no-image-icon.jpg',width: 70, height: 50, fit: BoxFit.fill,) : Image.network(this.classified.photo, width: 70,
        fit: BoxFit.fill, height: 50,),
        title: Text(substring_text(this.classified.title, 70),
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14)),
        subtitle: Column(
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: Icon(Icons.phone, size: 12, color: Colors.grey,),
                ),
                Text(this.classified.phone ?? 'No Contact', style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.red)),
              ],
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: Icon(Icons.access_time, size: 12, color: Colors.grey,),
                ),
                Text(formatDate(this.classified.created), style: Theme.of(context).textTheme.caption),
              ],
            ),
          ],
        ),
        onTap: () => Navigator.push(context, MaterialPageRoute(
          builder: (_) => ClassifiedDetailPage(item: this.classified,),
        )),
      ),
    );
  }

  Widget _card_text(BuildContext context){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(substring_text(this.classified.title, 50),
              style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Icon(Icons.phone, size: 12, color: Colors.grey,),
              ),
              Text(this.classified.phone ?? 'No Contact', style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.red)),
            ],
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Icon(Icons.access_time, size: 12, color: Colors.grey,),
              ),
              Text(formatDate(this.classified.created), style: Theme.of(context).textTheme.caption),
            ],
          ),

        ],
      ),
    );
  }

  Widget _photo(BuildContext context){
    var image_url = this.classified.photo == null ? AssetImage('assets/images/classic_car.jpg') : NetworkImage(this.classified.photo);
    return InkWell(
      child: Container(
          height: 80.0,
          child: SizedBox(width: 70.0,),
          decoration: BoxDecoration(
              image: DecorationImage(
                image: image_url,
                fit: BoxFit.fitHeight,
                alignment: Alignment.topLeft,
              )
          ) ,
        ),
        onTap: () => Navigator.push(context, MaterialPageRoute(
          builder: (_) => ClassifiedDetailPage(item: this.classified,),
        ))
    );
  }
}
