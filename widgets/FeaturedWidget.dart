
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ugalav/models/advert.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/screens/ad_detail_page.dart';
import 'package:ugalav/screens/classified_detail_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';

class FeaturedWidget extends StatefulWidget {
  @override
  _FeaturedWidgetState createState() => _FeaturedWidgetState();
}

class _FeaturedWidgetState extends State<FeaturedWidget> {
  Advert _featured;
  Timer timer;
  String next_url;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getFeature();
    timer = Timer.periodic(Duration(seconds: 15), (Timer t) => _getFeature());

  }

  @override
  Widget build(BuildContext context) {
    return _featured == null ? _loader() : Container(

        decoration: BoxDecoration(
            color: Colors.grey,
          border: Border(
            top: BorderSide(width: 1, color: Theme.of(context).primaryColor)
          )
        ),
        padding: EdgeInsets.only(right: 10, left: 10, top: 0, bottom: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(left: 8, right: 8),
              color: Theme.of(context).primaryColor,
              child: Text("-Advertisement -", style: TextStyle(color:Colors.yellow,),),
            ),
            SizedBox(height: 10,),
            //FeaturedItem()
            advertisement(),
            SizedBox(height: 10,),
          ],
        )
    );
  }

  Widget advertisement(){
     return Container(
       child: Row(
         mainAxisAlignment: MainAxisAlignment.start,
         children: [
           Flexible(child: _prev_arrow(), flex: 1,),
           Flexible(
             child: _photo(), flex: 2,
           ),
           Flexible(
             flex: 5,
             child: Column(
               mainAxisAlignment: MainAxisAlignment.start,
               crossAxisAlignment: CrossAxisAlignment.start,
               children: [
                 Padding(
                   padding: const EdgeInsets.only(left:8.0),
                   child: InkWell(
                     child: Text(substring_text(_featured.title, 80),
                       overflow:TextOverflow.ellipsis, maxLines: 4,
                       style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
                     ),
                     onTap: () => Navigator.push(context, MaterialPageRoute(
                       builder: (_) => AdvertDetailPage(item: _featured,),
                     )),
                   ),
                 ),
                 Row(
                   children: [
                     Padding(
                       padding: const EdgeInsets.only(right: 5, left: 8.0),
                       child: Icon(Icons.phone, size: 12, color: Colors.black26,),
                     ),
                     Text(_featured.phone,style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.red)),
                   ],
                 ),
               ],
             ),
           ),
           Flexible(child: _next_arrow(), flex: 1,),
         ],
       ),
     );
  }

  Widget _photo(){
    return _featured.photo == null ? Image.asset('assets/images/no-image-icon.jpg', width: 80,
      fit: BoxFit.fill,) : Image.network(_featured.photo, width: 80, height: 70,
    fit: BoxFit.cover,);
  }

  Widget _prev_arrow(){
     return _featured.previous == null ? Icon(Icons.arrow_back_ios):
              InkWell(child: Icon(Icons.arrow_back_ios),
                  onTap: ()=> _getPrevious(_featured.previous));
  }

  Widget _next_arrow(){
    return _featured.next == null ? Icon(Icons.arrow_forward_ios):
    InkWell(child: Icon(Icons.arrow_forward_ios),
        onTap: ()=> _getFeature());
  }
/*
  Widget FeaturedItem(){
    return Container(
        padding: EdgeInsets.all(1),
        child: ListTile(
          isThreeLine: true,
          dense: true,
          leading: InkWell(
            child: Icon(Icons.arrow_back),
            onTap: () =>  _getFeature(),
          ),
          title: InkWell(
            child: Text(substring_text(_featured.title, 60),
              overflow:TextOverflow.ellipsis, maxLines: 2,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
            ),
            onTap: () => Navigator.push(context, MaterialPageRoute(
              builder: (_) => ClassifiedDetailPage(item: _featured,),
            )),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(_featured.phone,style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.red)),
              Text(formatDate(_featured.created)),
            ],
          ),
          trailing: InkWell(
            child: Icon(Icons.arrow_forward),
            onTap: () =>  _getFeature(),
          ),

        )
    );
  }*/

  void _getFeature() async{
    if(this.mounted){
      setState(() {
        _featured = null;
      });
    }
    final Advert response = await fetchAdvert(this.next_url);
    this.next_url = response.next;
    if(this.mounted) {
      setState(() {
        _featured = response;
      });
    }
  }

  void _getPrevious(String url) async{
    if(this.mounted){
      setState(() {
        _featured = null;
      });
    }
    final Advert response = await fetchAdvert(url);
    this.next_url = response.next;
    if(this.mounted) {
      setState(() {
        _featured = response;
      });
    }
  }

  Widget _loader(){
    return Container(
      padding: EdgeInsets.all(30.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }
}
