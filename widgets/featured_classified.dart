
import 'package:flutter/material.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/screens/classified_detail_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';

class FeaturedClassified extends StatelessWidget {
  final Classified classified;
  FeaturedClassified(this.classified);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _photo(context),
            Expanded(child: InkWell(
              child: _card_text(context),
              onTap: () => Navigator.push(context, MaterialPageRoute(
                builder: (_) => ClassifiedDetailPage(item: this.classified,),
              )),
            ))
          ],
        ),
      ),
    );
  }

  Widget _card_text(BuildContext context){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(substring_text(this.classified.title, 50),
              style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Icon(Icons.phone, size: 12, color: Colors.grey,),
              ),
              Text(this.classified.phone ?? 'No Contact', style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.red)),
            ],
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Icon(Icons.access_time, size: 12, color: Colors.grey,),
              ),
              Text(formatDate(this.classified.created), style: Theme.of(context).textTheme.caption),
            ],
          ),

        ],
      ),
    );
  }

  Widget _photo(BuildContext context){
    var image_url = AssetImage('images/classic_car.jpg');
    return InkWell(
      child: Container(
        height: 80.0,
        child: SizedBox(width: 70.0,),
        decoration: BoxDecoration(
            image: DecorationImage(
              image: image_url,
              fit: BoxFit.fitHeight,
              alignment: Alignment.topLeft,
            )
        ) ,
      ),
       onTap: () => Navigator.push(context, MaterialPageRoute(
          builder: (_) => ClassifiedDetailPage(item: this.classified,),
        ))
    );
  }
}
