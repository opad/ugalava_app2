
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/screens/classified_detail_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';

class FeaturedWidget extends StatefulWidget {
  @override
  _FeaturedWidgetState createState() => _FeaturedWidgetState();
}

class _FeaturedWidgetState extends State<FeaturedWidget> {
  Classified _featured;
  Timer timer;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getFeature();
    timer = Timer.periodic(Duration(seconds: 15), (Timer t) => _getFeature());

  }

  @override
  Widget build(BuildContext context) {
    return _featured == null ? _loader() : Container(

        decoration: BoxDecoration(
            color: Colors.grey,
          border: Border(
            top: BorderSide(width: 2, color: Theme.of(context).primaryColor)
          )
        ),
        padding: EdgeInsets.only(right: 10, left: 10, top: 0, bottom: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(left: 8, right: 8),
              color: Theme.of(context).primaryColor,
              child: Text("-Advertisement -", style: TextStyle(color:Colors.yellow,),),
            ),
            SizedBox(height: 5,),
            FeaturedItem()
          ],
        )
    );
  }

  Widget FeaturedItem(){
    return Container(
        padding: EdgeInsets.all(1),
        child: ListTile(
          isThreeLine: true,
          dense: true,
          leading: _featured.photo == null ? Image.asset('assets/images/no-image-icon.jpg', width: 50,
            fit: BoxFit.fill,) : Image.network(_featured.photo, width: 70,
            fit: BoxFit.fill,),
          title: InkWell(
            child: Text(substring_text(_featured.title, 60),
              overflow:TextOverflow.ellipsis, maxLines: 2,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
            ),
            onTap: () => Navigator.push(context, MaterialPageRoute(
              builder: (_) => ClassifiedDetailPage(item: _featured,),
            )),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(_featured.phone,style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.red)),
              Text(formatDate(_featured.created)),
            ],
          ),
          trailing: InkWell(
            child: Icon(Icons.arrow_forward),
            onTap: () =>  _getFeature(),
          ),

        )
    );
  }

  void _getFeature() async{
    if(this.mounted){
      setState(() {
        _featured = null;
      });
    }
    final Classified response = await fetchFeatureClassified();
    if(this.mounted) {
      setState(() {
        _featured = response;
      });
    }
  }

  Widget _loader(){
    return Container(
      padding: EdgeInsets.all(30.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }
}
