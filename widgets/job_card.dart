import 'package:flutter/material.dart';
import 'package:ugalav/models/classified.dart';
import 'package:ugalav/models/classifieds.dart';
import 'package:ugalav/models/job.dart';
import 'package:ugalav/screens/classified_detail_page.dart';
import 'package:ugalav/screens/job_detail_page.dart';
import 'package:ugalav/utils/utility_funcs.dart';

class JobCard extends StatelessWidget {
  final Job job;
  JobCard(this.job);
  @override
  Widget build(BuildContext context) {
    var rate_period = job.rate_period != null ? '/' + job.rate_period: '';
    return Card(
      child: ListTile(
        isThreeLine: true,
        leading: this.job.photo == null ? Image.asset('assets/images/no-image-icon.jpg',width: 70, height: 50, fit: BoxFit.fill,) :
          Image.network(this.job.photo, width: 70, height: 50, fit: BoxFit.fill,),
        title: Text(this.job.title, style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.bold
        ),),
        subtitle: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('\$' + substring_text(this.job.price, 50) + rate_period,
                style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold)),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: Icon(Icons.phone, size: 12, color: Colors.grey,),
                ),
                Text(this.job.phone ?? 'No Contact', style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.red)),
              ],
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: Icon(Icons.access_time, size: 12, color: Colors.grey,),
                ),
                Text(formatDate(this.job.created), style: Theme.of(context).textTheme.caption),
              ],
            ),
          ],
        ),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.arrow_forward_ios),
          ],
        ),
        onTap: ()=>Navigator.push(context, MaterialPageRoute(
          builder: (_) => JobDetailPage(item: this.job,),
        )),
      ),
    );
  }
}
