import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ugalav/models/advert.dart';
import 'package:ugalav/models/job.dart';
import 'package:ugalav/models/listing_category.dart';
import 'package:ugalav/models/listings.dart';
import 'package:ugalav/models/locations.dart';
import 'package:ugalav/utils/constants.dart' as constants;
import 'package:ugalav/models/categories.dart';
import 'package:ugalav/models/classified.dart';

String formatDate(String date){
  final String dateFormatted = DateFormat("MMM dd, yyyy H:m:s").format(DateTime.parse(date));
  return dateFormatted;
}

void _onLoading(BuildContext context) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Dialog(
        child: new Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            new CircularProgressIndicator(),
            new Text("Loading"),
          ],
        ),
      );
    },
  );
}

String substring_text(String text, int len){
  if(text.length > len){
    return text.substring(0, len) + '...';
  }
  return text;
}

String validateEmail(String value) {
  Pattern pattern =
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
      r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
      r"{0,253}[a-zA-Z0-9])?)*$";
  RegExp regex = new RegExp(pattern);
  if (!regex.hasMatch(value) || value == null)
    return 'Enter a valid email address';
  else
    return null;
}


Future <List<Classified>> fetchClassifieds() async{
  var url = constants.BASE_URL + '/classifieds/?is_active=Y&limit=100';
  final response = await http.get(url);
  final parsed = jsonDecode(response.body)['results'].cast<Map<String, dynamic>>();
  return parsed.map<Classified>((json) => Classified.fromJson(json)).toList();

}

Future <int> deleteClassified(int classified) async{
  var url = constants.BASE_URL + '/user/classifieds/'+ classified.toString() + '/';
  final prefs = await SharedPreferences.getInstance();
  String storedUser = prefs.getString('jwt');
  Map<String, String> headers = {
    'Authorization': 'Bearer ' + storedUser
  };
  print(url);
  final response = await http.delete(url, headers: headers);
  print(response.statusCode);
  return response.statusCode;
}

Future <List<Listings>> fetchListing({String location, String q}) async{
  var url = constants.BASE_URL + '/listings/';
  if(location !=null && q !=null){
    url += '?state='+ location +'&search=' + q;
  }
  final response = await http.get(url);
  if(response.statusCode == 200){
    final parsed = jsonDecode(response.body)['results'].cast<Map<String, dynamic>>();
    return parsed.map<Listings>((json) => Listings.fromJson(json)).toList();
  }else{
    return [];
  }

}


Future <List<Listings>> fetchMyListing() async{
  var url = constants.BASE_URL + '/user/listings/';
  final prefs = await SharedPreferences.getInstance();
  String storedUser = prefs.getString('jwt');
  Map<String, String> headers = {
    'Authorization': 'Bearer ' + storedUser
  };
  final response = await http.get(url, headers: headers);
  if(response.statusCode == 200){
    final parsed = jsonDecode(response.body)['results'].cast<Map<String, dynamic>>();
    return parsed.map<Listings>((json) => Listings.fromJson(json)).toList();
  }else{
    return [];
  }

}

Future <List<Classified>> fetchMyClassifieds() async{
  var url = constants.BASE_URL + '/user/classifieds/';
  final prefs = await SharedPreferences.getInstance();
  String storedUser = prefs.getString('jwt');
  Map<String, String> headers = {
    'Authorization': 'Bearer ' + storedUser
  };
  final response = await http.get(url, headers: headers);
  if(response.statusCode == 200){
    final parsed = jsonDecode(response.body).cast<Map<String, dynamic>>();
    return parsed.map<Classified>((json) => Classified.fromJson(json)).toList();
  }else{
    return [];
  }

}

Future <int> deleteListing(int listing) async{
  var url = constants.BASE_URL + '/listings/'+ listing.toString() + '/';
  final prefs = await SharedPreferences.getInstance();
  String storedUser = prefs.getString('jwt');
  Map<String, String> headers = {
    'Authorization': 'Bearer ' + storedUser
  };
  final response = await http.delete(url, headers: headers);
  print(response.body);
  return response.statusCode;
}


Future <List<Job>> fetchJobs() async{
  var url = constants.BASE_URL + '/jobs/?is_active=Y&limit=100';
  final response = await http.get(url);
  final parsed = jsonDecode(response.body)['results'].cast<Map<String, dynamic>>();
  return parsed.map<Job>((json) => Job.fromJson(json)).toList();

}

Future <List<Category>> fetchCategories() async{
  var url = 'http://34.122.47.208/api/categories/';
  final response = await http.get(url);
  final parsed = jsonDecode(response.body).cast<Map<String, dynamic>>();

  return parsed.map<Category>((json) => Category.fromJson(json)).toList();

}

Future <List<ListingCategory>> fetchListingCategories() async{
  var url = 'http://34.122.47.208/api/listing-categories/';
  final response = await http.get(url);
  final parsed = jsonDecode(response.body).cast<Map<String, dynamic>>();
  return parsed.map<ListingCategory>((json) => ListingCategory.fromJson(json)).toList();

}

Future <List<Location>> fetchLocations() async{
  var url = 'http://34.122.47.208/api/listing-locations/';
  final response = await http.get(url);
  final parsed = jsonDecode(response.body).cast<Map<String, dynamic>>();
  return parsed.map<Location>((json) => Location.fromJson(json)).toList();

}

Future <Classified> fetchFeatureClassified() async{
  var url = constants.BASE_URL + '/featured/?limit=1';
  final response = await http.get(url);
  final parsed = jsonDecode(response.body)['results'].cast<Map<String, dynamic>>();
  if(parsed.length >=1){
     return Classified.fromJson(parsed[0]);
  }
}

Future <String> fetchSessionId(String item, int product) async{
  var url = constants.BASE_URL + '/checkout/?itemId=' + item +'&product='+ product.toString();
  final response = await http.get(url);
  if (response.statusCode == 200) {
    final parsed = jsonDecode(response.body);
    return parsed['session_id'];
  }
  return null;
}

Future <String> fetchAdSessionId(String item, int product) async{
  var url = constants.BASE_URL + '/advert_checkout/?itemId=' + item +'&product='+ product.toString();
  final response = await http.get(url);
  if (response.statusCode == 200) {
    final parsed = jsonDecode(response.body);
    return parsed['session_id'];
  }
  return null;
}

Future <Advert> fetchAdvert(String next_url) async{
  var url = (next_url == null) ? constants.BASE_URL + '/adverts/?limit=1': next_url;
  final response = await http.get(url);
  if (response.statusCode == 200) {
    final parsed = jsonDecode(response.body);
    var ad = parsed['results'].cast<Map<String, dynamic>>();
    Advert advert = Advert.fromJson(ad[0]);
    advert.prev_url = parsed['previous'];
    advert.next_url = parsed['next'];
    return advert;

  }
  return null;
}