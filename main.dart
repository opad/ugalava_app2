import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ugalav/models/app_state.dart';
import 'package:ugalav/screens/advert_success_page.dart';
import 'package:ugalav/screens/home_page.dart';
import 'package:ugalav/screens/listing_success_page.dart';
import 'package:ugalav/screens/loader.dart';
import 'package:ugalav/screens/login_page.dart';
import 'package:ugalav/screens/register_page.dart';
import 'package:ugalav/screens/success_page.dart';


void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Widget _defaultHome = Loader();
    return MaterialApp(
      title: 'Ugalav',
      home: _defaultHome,
      debugShowCheckedModeBanner: false,
      routes: {
        '/login': (BuildContext context) => LoginScreen(),
        '/register': (BuildContext context) => RegisterScreen(),
        '/home': (BuildContext context) => HomeScreen(),
        '/success': (BuildContext context) => SuccessPage(),
        '/success-listing': (BuildContext context) => SuccessListingPage(),
        '/success-advert': (BuildContext context) => AdvertSuccessPage(),
        //'/': (BuildContext context) => LoginScreen(),
      },
      theme: ThemeData(
        //brightness: Brightness.dark,
          primaryColor: Color.fromRGBO(255, 4, 4, 1),
          // accentColor: Color.fromRGBO(244, 230, 4, 1),
          accentColor: Colors.green,
          fontFamily: 'Trebuchet',
          textTheme: TextTheme(
              headline:
              TextStyle(fontSize: 36.0, fontWeight: FontWeight.bold),
              title: TextStyle(fontSize: 22.0),
              subtitle: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w500),
              body1: TextStyle(fontSize: 14.0))

      ),
    );
  }


  Future<bool> checkIsLogin() async {
    String _token = "";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = prefs.getString('jwt');
    if (_token != "" && _token != null) {
      return true;
    }
    return null;
  }

}